//
//  AppDelegate.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 03/11/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import UIKit
import Firebase
import SideMenuSwift
import FirebaseMessaging
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    static var DEVICEID = ""
    var sideMenu: SideMenuController?

    var window: UIWindow?
    var navigationController = UINavigationController()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        StripeManager.shared.setupStripe()

        if DataManager.chatID != nil || DataManager.isOnboardingDone {
            HomeVC.type = ScreenType(rawValue: DataManager.userType ?? ScreenType.driver.rawValue) ?? ScreenType.driver
            DataManager.isOnboardingDone ? loadLeftSideMenu() : loadChatScreen()
        } else {
            let storyboard = UIStoryboard(storyboard: .Main)
            let vc = storyboard.instantiateViewController(withIdentifier: ONBOARDING_VC) as! OnboardingVC
            navigationController.pushViewController(vc, animated: false)
            navigationController.setNavigationBarHidden(true, animated: false)
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = navigationController
            window?.makeKeyAndVisible()
        }

        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self

        let option: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: option, completionHandler: { (bool, err) in })

        application.registerForRemoteNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0

        return true
    }
}

//MARK: Appdelegate Navigations
extension AppDelegate {
    func loadLeftSideMenu() {
        var centerViewController = UIViewController()

        let storyboard = UIStoryboard(storyboard: .Home)
        let homevc = storyboard.instantiateViewController(withIdentifier: HOME_VC) as! HomeVC
        let sidevc = storyboard.instantiateViewController(withIdentifier: SIDE_MENU_VC) as! SideMenuVC
        centerViewController = UINavigationController(rootViewController: homevc)
        sideMenu = SideMenuController(contentViewController: centerViewController, menuViewController: sidevc)
        SideMenuController.preferences.basic.menuWidth = 300
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.layer.add(CATransition.transition, forKey: kCATransition)
        window?.rootViewController = sideMenu
        window?.makeKeyAndVisible()
    }

    func loadChatScreen() {
        let storyboard = UIStoryboard(storyboard: .Home)
        let chatvc = storyboard.instantiateViewController(withIdentifier: CHAT_VC) as! ChatVC
        chatvc.name = DataManager.name!
        chatvc.amount = DataManager.amount!
        chatvc.token = DataManager.token!
        navigationController.pushViewController(chatvc, animated: false)
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.layer.add(CATransition.transition, forKey: kCATransition)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase refresh registration token: \(fcmToken)")
        AppDelegate.DEVICEID = fcmToken
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let title = response.notification.request.content.title
        print(title)

        StripeManager.shared.setupStripe()

        Messaging.messaging().delegate = self

        UIApplication.shared.applicationIconBadgeNumber = 0

        if title == "New Request" {
            HomeVC.type = .driver
            NotificationCenter.default.post(name: .didGetNotification, object: nil)
            loadLeftSideMenu()
        } else if title == "New Message" {
            loadChatScreen()
        }
        completionHandler()
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        #if DEBUG
            Messaging.messaging().setAPNSToken(deviceToken as Data, type: .unknown)
        #else
            Messaging.messaging().setAPNSToken(deviceToken as Data, type: .prod)
        #endif
    }

    func connectToFCM() {
        NotificationCenter.default.post(name: .didGetToken, object: nil)
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        guard let token = fcmToken else { return }
        print("Firebase registration token: \(token)")
        AppDelegate.DEVICEID = token
    }
}
