//
//  APIKeys.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 11/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import Foundation

class APIKeys {
    
    //Driver Details
    static let kIsAvailable = "availability"
    static let kLatitude = "latitude"
    static let kLongitude = "longitude"
    static let kDeviceToken = "device_token"
    static let kShowMultipliedDriver = "kShowMultipliedDriver"
    static let kIsCancelled = "kIsCancelled"
    static let kIsAccepted = "kIsAccepted"
    static let kCount = "kCount"
    
    //USer Address Details
    static let kAddress = "Address"
    static let kZipCode = "Zip_Code"
    static let kCity = "City"
    static let kNameOnRing = "Name_On_Ring"
    static let kStripeId = "stripe_id"

    //Table Collections
    static let kDriverDetails = "Driver_Details"
    static let kUserAddress = "User_Address"
    
    //Card Details
    static let kCardNumber = "card_number"
    static let kCvv = "cvv"
    static let kExpiryDate = "expiry_date"
    
    
    //Stripe Constants
    static let kEmail = "email"
    static let kAuthorization = "Authorization"
    static let kMessage = "message"
    static let kId = "id"
    static let kSource = "source"
    static let kCustomer = "customer"
    static let kAmount = "amount"
    static let CURRENCY = "currency"
    static let kLast4 = "last4"
    static let kExpMonth = "exp_month"
    static let kExpYear = "exp_year"
    
    //Chat Details
    static let kDate = "date"
    static let kName = "name"
}
