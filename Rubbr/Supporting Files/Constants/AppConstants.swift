//
//  AppConstants.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 10/11/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
// ipdy-zsvg-chay-uzcz-aghq

import CoreLocation

//**** Validations ****
let EMAIL_CHECK = "[a-z0-9!#$%üäöß&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!üäöß#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
let ALPHA_NUMERIC_SET = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890 ")

let BASE_URL = "https://api.stripe.com/v1/"
let ALAMOFIRE_TIMEOUT: Double = 180

//let kStripeKey = "sk_test_NESfdzRlsjh6xJvb1A1N7eC5"
let STRIP_KEY = "sk_live_qoEgBFoA1kfIh4UXU894nWPC"

let kUser = "kUser"
let kDriver = "kDriver"

//**** Storyboard ID's ****
let HOME_VC = "HomeVC"
let PRICE_VC = "PriceVC"
let ONBOARDING_VC = "OnboardingVC"
let SIDE_MENU_VC = "SideMenuVC"
let USER_LOGIN_VC = "UserLoginVC"
let CHAT_VC = "ChatVC"
let ACCEPT_VC = "AcceptVC"
let TERMS_VC = "TermsVC"
let WEB_VIEW_VC = "WebViewVC"

//**** DataManagers Constants ****
let kIsOnboardingDone = "kIsOnboardingDone"
let kUserId = "kUserId"
let USER_TYPE = "userType"
let kStripeId = "kStripeId"
let kCardId = "kCardId"
let kUserName = "kUserName"
let kUserAddress = "kUserAddress"
let kChatID = "kChatID"
let kAmount = "kAmount"
let kName = "kName"
let kToken = "kToken"
let kAvailtability = "kAvailtability"
let kLatitude = "kLatitude"
let kLongitude = "kLongitude"
let kShowMultipliedDriver = "kShowMultipliedDriver"
let kIsDelivering = "kIsDelivering"




// =====================================================================================
let CURRENCY_EUR = "eur"
let LEAVE_FEEDBACK = "Leave Feedback"
let DRIVERS = "Drivers"
let EMAIL_ADDRESS = "piercjep@gmail.com"
let EMAIL_CONTENT = "<p>Please let us know, if you want to update anything in this or let us know your queries:</p>"

//**** Side menu items ****
let USER_LOGIN_ARRAY = ["Tell A Friend", "Address", "Payment Details", "Terms & Conditions", "Contact Us", "Leave Feedback"]
let DRIVER_ARAY = ["Tell A Friend", "Terms & Conditions", "Contact Us", "Leave Feedback"]
let ADDRESS_PLACEHOLDER_ARRAY = ["Address", "Zip Code", "City", "Name on ring bell"]

//**** Common Alerts ****
let kLocationMessage = "Please enable your location, to access the application."
let kLogoutMessage = "Are you sure you want to logout?"

let ENABLE_LOCATION_FROM_SETTINGS = "Enable location from settings"
let OKAY = "Okay"
let INFORMATION = "Information"
let ERROR = "Error"
let UNABLE_TO_PROCEED_YOUR_REQUEST = "Unable to proceed your request"
let YES = "Yes"
let NO = "No"
let JOIN_RUUBR_NOW = "Join Rubbr now, follow the link to join the Rubbr now: https://itunes.apple.com/de/app/myapp/id1438631063?ls=1&mt=8"

//**** Login Alerts ****
let kEmailValidation = "Please enter valid email address."
let kPasswordValidation = "Password must be at least 6 characters long."
let kNewPasswordValidation = "New password must be at least 6 characters long."
let kConfirmPasswordValidation = "Confirm Password does not match."
let kTermsValidation = "Please agree terms and conditions."

//**** Signup Alerts ****
let kNameValidation = "Name must be at least 2 characters long."
let kSignupSuccess = "You have successfully signed up. Please verify your email address to login into the application."
let kSocialSignupSuccess = "You have successfully signed up."
let kDescriptionValidation = "Description must be at least 4 characters long."
let kAddressValidation = "Please enter address."
let kZipValidation = "Please enter valid zip code."
let kCityValidation = "Please enter city."
let kCardValidation = "Please enter valid card number."
let kCvvValidation = "Please enter valid cvv number."
let kExpiryDateValidation = "Please select expiration date."

//**** Forgot Password Alerts ****
let kForgotEmailSent = "Password reset link has been sent to your resgistered email address."
let kNoUser = "There is no user record corresponding to this identifier. The user may have been deleted."

//**** Common Keys ****
let kSuccess = "Success"
let kRequests = "Requests"
let kChat = "Chat"

//**** HomeVC ****
let kAvailableTitle = "You are Available"
let kAvailableDescription = "You can toggle on and off whenever you see fit."
let kUnAvailableTitle  = "You are Unavailable"
let kUnAvailableDescription = "Mark yourself as available for delivery to receive requests."
let kNoDriversAvailable = "No drivers are available in your area."
let kUnsupportedLocation = "For the time being, Rubbr doesn't operate in your area. Thanks for your patience while we get bigger"

//**** WebView ****
let kLeaveFeedbackURL = "https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.surveymonkey.de%2Fr%2F7H7K76H%3Ffbclid%3DIwAR0JAksEHLh2pqMFyZhiaPNoA-RsG7PknkXd_YmOo2uCE3fFAmsuaEU05wA&h=AT07oF2LnuZ05JBE_dVmeT2OYO_TPifOgm1zk0rVyHO0cgPHV180Q6CeuHCvF-E8XipPU5inTHLqjRZ-6MV4RVz6RL9L7A9GrK8zW0AfOs_0Or6RoRVoEDf2XIBu7xX-U6VEy6-sUK4"
let NOTIFICATION_URL = "https://fcm.googleapis.com/fcm/send"
let SERVERKEY = "AAAAv5RPfo4:APA91bF68A4OxOZG7xwjQ0jT65ifVnPLeJy3ai9zZsxcMtI8SWQeMy1QVVJwazJupdHyDISoxW7prPDSizULrJ1Yqbk--EwFfooEWHQovLwtuq7DsJlOUTXnyrwhIUHeNgDYIlSaOkdn"

//**** Fields Name ****
let kEmailFieldPlaceholder = "Email"
let kPassowrdFieldPlaceholder = "Password"
let kCardNumberFieldPlaceholder = "Card Number"
let k3DigitNumberFieldPlaceholder = "3 digit Number"
let kExpirationDateFieldPlaceholder = "Expiration Date"

let SUPPORTED_REGIONS = [
    CLCircularRegion(center: CLLocationCoordinate2D(latitude: 52.515816,
                                                    longitude: 13.454293),
                     radius: 3000,
                     identifier: "Friedrichshain"),
    CLCircularRegion(center: CLLocationCoordinate2D(latitude: 52.498604,
                                                    longitude: 13.391799),
                     radius: 3000,
                     identifier: "Kreuzberg"),
    CLCircularRegion(center: CLLocationCoordinate2D(latitude: 52.53878,
                                                    longitude: 13.42443),
                     radius: 3000,
                     identifier: "Prenzlauer Berg"),
    CLCircularRegion(center: CLLocationCoordinate2D(latitude: 52.520008,
                                                    longitude: 13.404954),
                     radius: 3000,
                     identifier: "Neukölln"),
    CLCircularRegion(center: CLLocationCoordinate2D(latitude: 52.531677,
                                                    longitude: 13.381777),
                     radius: 3000,
                     identifier: "Mitte")
]
