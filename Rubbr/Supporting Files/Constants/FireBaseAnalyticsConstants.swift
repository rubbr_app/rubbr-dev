//
//  FireBaseAnalyticsConstants.swift
//  Rubbr
//
//  Created by MacOS on 3/10/19.
//  Copyright © 2019 Hamlet Kosakyan. All rights reserved.
//

import Foundation

let kSignUpButtonClicked = "UserSignedUp"
let kLoginButtonClicked = "UserLoggedIn"
let kLoggedOut = "Loggedout"
let kRubbrOrdered = "RubbrOrdered"
let kLocationDetected = "LocationDetected"
let kPriceSelected = "SelectedPrice"
let kTellAFriend = "TellAFriendTapped"
let kTermsAndConditionsOpened = "TermsAndConditionsOpened"
let kContactUs = "ContactUsTapped"
let kOutOfRegions = "UserIsOutOfSupportedRegions"
let kLeaveFeedbackTapped = "LeaveFeedbackTapped"
let kEmailEntered = "EmailFieldEntered"
let kPasswordEntered = "PasswordFieldEntered"
let kAddressEntered = "AddressFieldEntered"
let kZipCodeEntered = "ZipCodeEntered"
let kCityEntered = "CityEntered"
let kNameOnRingBellEntered = "NameOnRingBellEntered"
let kCardNumberEntered = "CardNumberEntered"
let kCardExpirationDateEntered = "CardExpirationDateEntered"
let kCardCVVEntered = "CardCVVEntered"
