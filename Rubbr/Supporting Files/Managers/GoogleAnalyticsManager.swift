//
//  GoogleAnalyticsManager.swift
//  Rubbr
//
//  Created by MacOS on 3/6/19.
//  Copyright © 2019 Hamlet Kosakyan. All rights reserved.
//

import Foundation
import Firebase

class GoogleAnalytics: NSObject {
    static func logEvent(event: String, parameters: [String: String]?) {
        let replaced = event.replacingOccurrences(of: " ", with: "_").replacingOccurrences(of: ":", with: "_")
        if replaced.count > 40 {
            print("too much characters in the event name.")
            return
        }
        
        Analytics.logEvent(replaced, parameters: parameters)
    }
}
