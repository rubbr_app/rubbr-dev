//
//  StripeManager.swift
//
//  Created by Apple on 09/08/18.
//  Copyright © 2018 Manvik. All rights reserved.
//

import Foundation
import Stripe

typealias stripeCallBack = ((String?, Error?) -> ())

class StripeManager: NSObject {
    private var STRIPE_KEY = "pk_live_q76t99o3OlCmW1WhRxaIJD71" // pk_test_BJYZ0VPTm7cqkjJykPKr4AI2"
    static let shared = StripeManager()
    
    func setupStripe() {
        STPAPIClient.shared.publishableKey = STRIPE_KEY
    }
    
    func saveCard(cardNumber: String, expMonth: Int, expYear: Int, cvv: String, success: @escaping stripeCallBack) {
        let cardParams = STPCardParams()
        cardParams.number = cardNumber
        cardParams.expMonth = UInt(expMonth)
        cardParams.expYear = UInt(expYear)
        cardParams.cvc = cvv
        
        Indicator.shared.showIndicator()
        STPAPIClient.shared.createToken(withCard: cardParams) { (token, error) in
            Indicator.shared.hideIndicator()
            if error != nil {
                success(nil, error)
            } else {
                success(token!.tokenId, nil)
            }
        }
    }
}
