//
//  DataManager.swift
//  CRSI
//
//  Created by Apple on 11/07/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import Foundation

class DataManager {

    static var isOnboardingDone: Bool {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kIsOnboardingDone)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.bool(forKey: kIsOnboardingDone)
        }
    }

    static var userID: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kUserId)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kUserId)
        }
    }

    static var userType: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: USER_TYPE)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: USER_TYPE)
        }
    }

    static var stripeId: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kStripeId)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kStripeId)
        }
    }

    static var cardId: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kCardId)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kCardId)
        }
    }

    static var userName: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kUserName)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kUserName)
        }
    }

    static var userAddress: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kUserAddress)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kUserAddress)
        }
    }

    static var chatID: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kChatID)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kChatID)
        }
    }

    static var amount: Double? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kAmount)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.double(forKey: kAmount)
        }
    }

    static var name: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kName)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kName)
        }
    }

    static var token: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kToken)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kToken)
        }
    }

    static var availability: Bool? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kAvailtability)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.bool(forKey: kAvailtability)
        }
    }

    static var latitude: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kLatitude)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kLatitude)
        }
    }

    static var longitude: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kLongitude)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kLongitude)
        }
    }

    static var showMultipliedDriver: Bool? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kShowMultipliedDriver)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.bool(forKey: kShowMultipliedDriver)
        }
    }

    static var isDelivering: Bool {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kIsDelivering)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.bool(forKey: kIsDelivering)
        }
    }
    
    static func rmeoveOldValues() {
        isOnboardingDone = false
        cardId = nil
        stripeId = nil
        userID = nil
        userAddress = nil
    }
}
