//
//  NetworkManager.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 01/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import Foundation
import Alamofire

//MARK: Call Backs
typealias JSONDictionary = [String: Any]
typealias JSONArray = [JSONDictionary]
typealias APIServiceSuccessCallback = ((Any?) -> ())
typealias APIServiceFailureCallback = ((NetworkErrorReason?, Error?) -> ())
typealias JSONArrayResponseCallback = ((JSONArray?) -> ())
typealias JSONDictionaryResponseCallback = ((JSONDictionary) -> ())
typealias responseCallBack = ((String?, Error?) -> ())

//MARK: Constants
var myRequest: Alamofire.Request?

//MARK: Enums
public enum NetworkErrorReason: Error {
    case FailureErrorCode(code: Int, message: String)
    case InternetNotReachable
    case Other
}

struct Resource {
    let method: HTTPMethod
    let parameters: [String: Any]?
    let headers: HTTPHeaders?
    
    init(method: HTTPMethod, parameters: [String: Any]? = nil, headers: HTTPHeaders = [APIKeys.kAuthorization: "Bearer \(STRIP_KEY)"]) {
        self.method = method
        self.parameters = parameters
        self.headers = headers
    }
}

protocol APIService {
    var path: String? { get }
    var resource: Resource? { get }
}

class APIManager {
    //MARK: Variables
    static let kMessage = "message"
    static let NO_INTERNET = "No Internet"
    static let INTERNET_ERROR = "Your internet connection appears to be offline. Please try again."
    static let SERVER_ERROR = "Server Error"
    static let OTHER_ERROR = "Unable to connect with server. Please try again."
    static let ERROR = "Error"
    static let SOMETHING_WRONG = "Something went wrong. Please try again"
    static let UNAUTHORIZED = "Session expired. Please login again."

    class func errorForNetworkErrorReason(errorReason: NetworkErrorReason) -> (NSError) {
        var error: NSError!
        switch errorReason {
        case .InternetNotReachable:
            error = NSError(domain: APIManager.NO_INTERNET, code: -1, userInfo: [APIManager.kMessage: APIManager.INTERNET_ERROR])
        case let .FailureErrorCode(code, message):
            switch code {
            case 400, 401, 402, 403:
                error = NSError(domain: APIManager.ERROR, code: code, userInfo: [APIManager.kMessage: message])
            case 500:
                error = NSError(domain: APIManager.SERVER_ERROR, code: code, userInfo: [APIManager.kMessage: SERVER_ERROR])
            default:
                error = NSError(domain: APIManager.OTHER_ERROR, code: code, userInfo: [APIManager.kMessage: message])
            }
        case .Other:
            error = NSError(domain: APIManager.OTHER_ERROR, code: 0, userInfo: [APIManager.kMessage: APIManager.SOMETHING_WRONG])
        }
        return error
    }
}

extension APIService {
    /*
     Method which needs to be called from the respective model class.
     - parameter successCallback:   successCallback with the JSON response.
     - parameter failureCallback:   failureCallback with ErrorReason, Error description and Error.
     */

    //MARK: Request Method to Send Request
    func request(isJsonRequest: Bool = false, success: @escaping APIServiceSuccessCallback, failure: @escaping APIServiceFailureCallback) {
        if isJsonRequest {
            self.jsonRequest(jsonSuccess: { (responseSuccess) in
                success(responseSuccess)
            }, jsonFailure: { (errorReason, error) in
                failure(errorReason, error)
            })
        } else {
            self.requestNormal(normalSuccess: { (responseSuccess) in
                success(responseSuccess)
            }, normalFailure: { (errorReason, error) in
                failure(errorReason, error)
            })
        }
    }

    func request(imageDict: [String: Data]?, videoDict: [String: Data]? = nil, isArray: Bool = false, imageName: String? = nil, videoName: String? = nil, success: @escaping APIServiceSuccessCallback, failure: @escaping APIServiceFailureCallback) {

        showIndicator() //Show Indicator
        showRequest() //Show Request in Console

        //Set Data
        let urlRequest = createMultipartRequest(imageDict: imageDict, videoDict: videoDict, isArray: isArray, imageName: imageName, videoName: videoName)

        //Create Request
        AF.upload((urlRequest?.1)!, with: (urlRequest?.0)!).uploadProgress(closure: { (progress) in
            print(progress.localizedDescription ?? "")
        }).responseJSON(completionHandler: { (response) in

            //Handle Indicators
            Indicator.shared.hideIndicator()
            myRequest = nil

            //Show Response Details
            debugPrint("*** API RESPONSE START ***")
            debugPrint("\(response))")
            debugPrint("*** API RESPONSE END ***")

            //Handle Response
            self.handleResponse(response: response, responseSuccess: { (responseSuccess) in
                success(responseSuccess)
            }, responseFailure: { (errorReason, error) in
                failure(errorReason, error)
            })
        })
    }

    //MARK: JSON REUQEST
    private func jsonRequest(jsonSuccess: @escaping APIServiceSuccessCallback, jsonFailure: @escaping APIServiceFailureCallback) {
        do {
            guard let finalPath = path, let url = URL(string: finalPath), let resource = resource else { return }
            showIndicator()
            showRequest()

            var request = URLRequest(url: url)
            request.httpMethod = resource.method.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            if let headers = resource.headers {
                for header in headers {
                    request.setValue(header.value, forHTTPHeaderField: header.name)
                }
            }

            //Set Parameteres
            if let parameters = resource.parameters {
                request.httpBody = try! JSONSerialization.data(withJSONObject: parameters)
            }

            //Create Request
            AF.session.configuration.timeoutIntervalForRequest = ALAMOFIRE_TIMEOUT
            myRequest = AF.request(request).responseJSON { response in
                Indicator.shared.hideIndicator()
                myRequest = nil
                showResponse(response: response)
                
                self.handleResponse(response: response, responseSuccess: { (success) in
                    jsonSuccess(success)
                }, responseFailure: { (errorReason, error) in
                    jsonFailure(errorReason, error)
                })
            }
        }
    }

    //MARK: NORMAL REUQEST
    private func requestNormal(normalSuccess: @escaping APIServiceSuccessCallback, normalFailure: @escaping APIServiceFailureCallback) {
        guard let finalPath = path, let resource = resource else { return }
        showIndicator()
        showRequest()

        AF.request(finalPath, method: resource.method, parameters: resource.parameters, encoding: URLEncoding.default, headers: resource.headers).validate().responseJSON(completionHandler: { (response) in

            Indicator.shared.hideIndicator()
            
            self.handleResponse(response: response, responseSuccess: { (success) in
                normalSuccess(success)
            }, responseFailure: { (errorReason, error) in
                normalFailure(errorReason, error)
            })
        })
    }

    //MARK: MULTIPART REQUEST
    private func createMultipartRequest(imageDict: [String: Data]?, videoDict: [String: Data]?, isArray: Bool, imageName: String?, videoName: String?) -> (URLRequestConvertible, Data)? {

        guard let finalPath = path, let url = URL(string: finalPath), let resource = resource else { return nil }
        
        // Create URL Request
        var mutableURLRequest = URLRequest(url: url)
        mutableURLRequest.httpMethod = resource.method.rawValue
        
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary=" + boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        var uploadData = Data()

        // Set Parameters
        if let parameters = resource.parameters {
            for (key, value) in parameters {
                if let boundaryConstantData = "--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8) {
                    uploadData.append(boundaryConstantData)
                }
                
                if let contentDisposition = "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8) {
                    uploadData.append(contentDisposition)
                }
                
                if let valueData = "\(value)\r\n".data(using: String.Encoding.utf8) {
                    uploadData.append(valueData)
                }
            }
        }

        //Set Headers
        if let headers = resource.headers {
            for header in headers {
                mutableURLRequest.setValue(header.value, forHTTPHeaderField: header.name)
            }
        }

        //Set Images
        if let imageDict = imageDict {
            for (key, value) in imageDict {
                
                if let boundaryConstant = "--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8) {
                    uploadData.append(boundaryConstant)
                }

                if let contentDisposition = isArray ? "Content-Disposition: form-data; name=\"\(imageName!)[]\"; filename=\"\(imageName!).\(value.mimeType)\"\r\n".data(using: String.Encoding.utf8) :
                    "Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(key).\(value.mimeType)\"\r\n".data(using: String.Encoding.utf8) {
                    uploadData.append(contentDisposition)
                }
                
                if let contentType = "Content-Type: image/png\r\n\r\n".data(using: String.Encoding.utf8) {
                    uploadData.append(contentType)
                }
                
                uploadData.append(value)
                
                if let lastValue = "\r\n".data(using: String.Encoding.utf8) {
                    uploadData.append(lastValue)
                }
            }
        }

        //Set Videos
        if let videoDict = videoDict {
            for (key, value) in videoDict {
                if let boundaryConstant = "--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8) {
                    uploadData.append(boundaryConstant)
                }
                
                if let contentDisposition = isArray ? "Content-Disposition: form-data; name=\"\(videoName!)[]\"; filename=\"\(videoName!).\(value.mimeType)\"\r\n".data(using: String.Encoding.utf8) :
                    "Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(key).\(value.mimeType)\"\r\n".data(using: String.Encoding.utf8) {
                    uploadData.append(contentDisposition)
                }
                
                if let contentType = "Content-Type: image/png\r\n\r\n".data(using: String.Encoding.utf8) {
                    uploadData.append(contentType)
                }
                
                uploadData.append(value)
                
                if let lastValue = "\r\n".data(using: String.Encoding.utf8) {
                    uploadData.append(lastValue)
                }
            }
        }
        
        if let boundaryConstant = "--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8) {
            uploadData.append(boundaryConstant)
        }

        do {
            let result = try Alamofire.URLEncoding.default.encode(mutableURLRequest, with: nil)
            return (result, uploadData)
        } catch _ { }
        return nil
    }

    //MARK: Response Handling
    func handleResponse(response: AFDataResponse<Any>, responseSuccess: @escaping APIServiceSuccessCallback, responseFailure: @escaping APIServiceFailureCallback) {

        showResponse(response: response)

        switch response.result {
        case .success(let value):
            responseSuccess(value)
        case .failure(let error):
            self.handleError(response: response, error: error, callback: responseFailure)
        }
    }

    //MARK: Error Handling
    private func handleError(response: AFDataResponse<Any>?, error: Error?, callback: APIServiceFailureCallback) {
        guard let response = response else { return }
        if let errorCode = response.response?.statusCode {
            guard let responseData = response.data, let responseJSON = self.JSONFromData(data: responseData as NSData) else {
                callback(NetworkErrorReason.FailureErrorCode(code: errorCode, message: ""), error)
                debugPrint("Couldn't read the data")
                return
            }
            let message = (responseJSON as? NSDictionary)?["message"] as? String ?? "Something went wrong. Please try again."
            callback(NetworkErrorReason.FailureErrorCode(code: errorCode, message: message), error)
        } else {
            let customError = NSError(domain: "Network Error", code: (error! as NSError).code, userInfo: (error! as NSError).userInfo)
                switch response.result {
                case .success(_):
                    break
                case .failure(let error):
                    if error.localizedDescription == "The Internet connection appears to be offline." {
                        callback(NetworkErrorReason.InternetNotReachable, customError)
                    } else {
                        callback(NetworkErrorReason.Other, customError)
                    }
                }
        }
    }

    //MARK: Show Console Data
    func showRequest() {
        debugPrint("*** API REQUEST START ***")
        debugPrint("Request URL: \(path ?? "")")
        debugPrint("Request HTTP Method: \(resource?.method.rawValue ?? "")")
        debugPrint("Request Parameters: \(String(describing: resource?.parameters)))")
        debugPrint("Request Headers: \(String(describing: resource?.headers)))")
        debugPrint("*** API REQUEST END ***")
    }

    func showResponse(response: AFDataResponse<Any>) {
        debugPrint("*** API RESPONSE START ***")
        debugPrint("\(response))")
        debugPrint("*** API RESPONSE END ***")
        
        let code = response.response?.statusCode ?? 0
        debugPrint("*** API RESPONSE CODE: \(code) ***")
    }
    
    func showIndicator() {
        if Indicator.isEnabledIndicator {
            Indicator.shared.showIndicator()
        }
    }

    //MARK: Data Handling
    private func JSONFromData(data: NSData) -> Any? { //Convert Data to JSON
        do {
            return try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers)
        } catch let myJSONError {
            debugPrint(myJSONError)
        }
        return nil
    }

    private func nsdataFromJSON(json: AnyObject) -> NSData? { // Convert from JSON to nsdata
        do {
            return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData?
        } catch let myJSONError {
            debugPrint(myJSONError)
        }
        return nil;
    }
}

//MARK: Indicator Class
public class Indicator {
    public static let shared = Indicator()
    var blurImg = UIImageView()
    var indicator = UIActivityIndicatorView()
    static var isEnabledIndicator = true

    private init() {
        blurImg.frame = UIScreen.main.bounds
        blurImg.backgroundColor = UIColor.black
        blurImg.isUserInteractionEnabled = true
        blurImg.alpha = 0.5
        indicator.style = UIActivityIndicatorView.Style.large
        indicator.center = blurImg.center
        indicator.startAnimating()
        indicator.color = UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }

    func showIndicator() {
        DispatchQueue.main.async(execute: {
            UIApplication.shared.delegate?.window??.rootViewController?.view.addSubview(self.blurImg)
            UIApplication.shared.delegate?.window??.rootViewController?.view.addSubview(self.indicator)
        })
    }

    func hideIndicator() {
        DispatchQueue.main.async(execute: {
            self.blurImg.removeFromSuperview()
            self.indicator.removeFromSuperview()
        })
    }
}

//MARK: Get Mime Type
extension Data {
    private static let mimeTypeSignatures: [UInt8: String] = [
        0xFF: "jpeg",
        0x89: "png",
        0x47: "gif",
        0x49: "tiff",
        0x4D: "tiff",
        0x66: "3gp",
        0x52: "wav",
        0x00: "mpeg",
    ]

    var mimeType: String {
        var c: UInt8 = 0
        copyBytes(to: &c, count: 1)
        return Data.mimeTypeSignatures[c] ?? "mov"
    }
}
