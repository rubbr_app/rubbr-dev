//
//  UIColor.swift
//  HealthSplash
//
//  Created by Apple on 16/11/17.
//  Copyright © 2017 Deftsfot. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    enum RubbrColor  {
        case purpleMain
        case pinkMain
        case golden
        case orange
        case placeholder
        case borderColor
        
        func color(alpha: CGFloat = 1.0) -> UIColor {
            var colorToReturn:UIColor?
            switch self {
            case .purpleMain:
                colorToReturn = UIColor(red: 48/255, green: 35/255, blue: 174/255, alpha: alpha)
                
            case .pinkMain:
                colorToReturn = UIColor(red: 200/255, green: 109/255, blue: 215/255, alpha: alpha)
                
            case .golden:
                colorToReturn = UIColor(red: 250/255, green: 217/255, blue: 97/255, alpha: alpha)
                
            case .orange:
                colorToReturn = UIColor(red: 247/255, green: 107/255, blue: 28/255, alpha: alpha)
                
            case .placeholder:
                colorToReturn = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: alpha)
                
            case .borderColor:
                colorToReturn = UIColor(red: 208/255, green: 208/255, blue: 208/255, alpha: alpha)
                
            }
            return colorToReturn!
        }
    }
    
    //TO REMOVE THE NAVIGATION BAR LINE
    func as1ptImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        setFill()
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return image
    }
}



