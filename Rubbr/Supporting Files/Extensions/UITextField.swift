//
//  UITextField.swift
//  Vits Video Calling Interpreter
//
//  Created by Apple on 07/08/17.
//  Copyright © 2017 Deftsoft. All rights reserved.
//


import UIKit

extension UITextField {
    var isEmpty: Bool {
        return text?.trimmingCharacters(in: .whitespaces).count == 0
    }
    
    var count: Int {
        return text?.count ?? 0
    }
    
    func setPlaceholder(placholder: String, color: UIColor = UIColor.RubbrColor.placeholder.color(), size: CGFloat = 14.0, style: UIFont.MyFont = UIFont.MyFont.regular) {
        
        let myPlaceholder = placholder
        let attributedString = NSAttributedString(string: myPlaceholder, attributes:[NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.font: style.fontWithSize(size: size)])
        tintColor = color
        font = style.fontWithSize(size: size)
        attributedPlaceholder = attributedString
    }
    
    //MARK: Validations
    var isValidEmail: Bool {
        let emailRegEx = EMAIL_CHECK
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: text!.lowercased())
    }

    var isValidPassword: Bool {
        return text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 >= 6
    }
    
    var isValidName: Bool {
        return text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 >= 2
    }
    
    var isValidCardNumber: Bool {
        return text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 >= 8
    }
    
    var isValidCVV: Bool {
        return text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 >= 3
    }
    
    var isValidAccountNumber: Bool {
        return text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 >= 7
    }
    
    var isValidBSBNumber: Bool {
        return text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 >= 3
    }
}

class TextField: UITextField {
    let padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15);
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}




