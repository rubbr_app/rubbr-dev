//
//  UIViewController.swift
//  Rubbr
//
//  Created by Hamlet on 20.03.21.
//  Copyright © 2021 Hamlet Kosakyan. All rights reserved.
//

import UIKit
import SideMenuSwift

public extension UIViewController {
    /// Access the nearest ancestor view controller  hierarchy that is a side menu controller.
    var sideMenuController: SideMenuController? {
        return findSideMenuController(from: self)
    }

    fileprivate func findSideMenuController(from viewController: UIViewController) -> SideMenuController? {
        var sourceViewController: UIViewController? = viewController
        repeat {
            sourceViewController = sourceViewController?.parent
            if let sideMenuController = sourceViewController as? SideMenuController {
                return sideMenuController
            }
        } while (sourceViewController != nil)
        return nil
    }
}
