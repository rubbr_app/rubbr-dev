//
//  UIFont
//
//
//  Created by Manish on 07/07/16.
//  Copyright © 2016 Manish. All rights reserved.
//

import UIKit

extension UIFont {
  enum MyFont: String {
    case regular = "SFProText-Regular"
    case medium = "SFProText-SemiBold"
    case bold = "SFProText-Bold"
    
    func fontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: rawValue, size: size)!
    }
  }
}
