//
//  UIView.swift
//  HealthSplash
//
//  Created by Apple on 16/11/17.
//  Copyright © 2017 Deftsfot. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    static var identifier: String {
        return String(describing: self)
    }
    
    var half: CGFloat {
        return frame.height/2
    }
    
    func set(radius: CGFloat, borderColor: UIColor = UIColor.clear, borderWidth: CGFloat = 0.0) {
        layer.cornerRadius = radius
        layer.masksToBounds = true
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
    }
    
    func setShadow() {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 2
        layer.shadowOpacity = 0.3
    }
    
    func generateShadowUsingBezierPath(radius: CGFloat)  {
        layer.cornerRadius = radius
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.clear.cgColor
        layer.masksToBounds = true
        
        let shadow = UIView(frame: frame)
        shadow.backgroundColor = .white
        shadow.isUserInteractionEnabled = false
        shadow.layer.shadowColor = UIColor.black.cgColor
        shadow.layer.shadowOffset = .zero
        shadow.layer.shadowRadius = 2.0
        shadow.layer.masksToBounds = false
        shadow.layer.cornerRadius = layer.cornerRadius
        shadow.layer.shadowOpacity = 0.3
        superview?.addSubview(shadow)
        superview?.sendSubviewToBack(shadow)
    }
    
    func setGradient(color1: UIColor, color2: UIColor, frame: CGRect, type: String = "horizontal") {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ color1.cgColor, color2.cgColor]
        
        if type == "horizontal" {
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        } else {
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
        }

        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = frame
        layer.sublayers?.insert(gradientLayer, at: 0)
    }
    
    func fadeIn(duration: TimeInterval = 0.3) {
        UIView.animate(withDuration: duration, animations: { [weak self] in
            self?.alpha = 1.0
        })
    }
    
    func fadeOut(duration: TimeInterval = 1.0) {
        UIView.animate(withDuration: duration, animations: { [weak self] in
            self?.alpha = 0.0
        })
    }
}
