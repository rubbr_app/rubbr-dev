//
//  Notification.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 25/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let acceptNotification = Notification.Name(
        rawValue: "kAcceptNotification")
    static let didGetNotification = Notification.Name("didGetNotification")
    static let didGetToken = Notification.Name("didGetToken")
}
