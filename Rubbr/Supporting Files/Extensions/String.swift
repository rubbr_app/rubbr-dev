//
//  String.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 01/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import Foundation

extension String {
    //MARK: Validations
    var isValidEmail: Bool {
        let emailRegEx = EMAIL_CHECK
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: lowercased())
    }
    
    var isValidPassword: Bool {
        return trimmingCharacters(in: .whitespacesAndNewlines).count >= 6
    }
    
    var isValidName: Bool {
        return trimmingCharacters(in: .whitespacesAndNewlines).count >= 2
    }

    var isValidCardNumber: Bool {
        return trimmingCharacters(in: .whitespacesAndNewlines).count >= 8
    }
    
    var isValidCVV: Bool {
        return trimmingCharacters(in: .whitespacesAndNewlines).count >= 3
    }
    
    var isValidAccountNumber: Bool {
        return trimmingCharacters(in: .whitespacesAndNewlines).count >= 7
    }
    
    var isValidBSBNumber: Bool {
        return trimmingCharacters(in: .whitespacesAndNewlines).count >= 3
    }
    
    var isValidZipCode: Bool {
        return trimmingCharacters(in: .whitespacesAndNewlines).count >= 4
    }
}
