//
//  CustomCatTransition.swift
//  Rubbr
//
//  Created by Hamlet on 20.03.21.
//  Copyright © 2021 Hamlet Kosakyan. All rights reserved.
//

import UIKit

extension CATransition {
    static var transition: CATransition {
        let transition = CATransition()
        transition.type = CATransitionType.fade
        transition.duration = 0.2
        return transition
    }
}
