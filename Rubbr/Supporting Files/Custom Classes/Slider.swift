//
//  Slider.swift
//  Rubbr
//
//  Created by Hamlet on 17.03.21.
//  Copyright © 2021 Hamlet Kosakyan. All rights reserved.
//

import UIKit

class Slider : UISlider {
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        let customBounds = CGRect(origin: bounds.origin, size: CGSize(width: bounds.size.width, height: 5.0))
        super.trackRect(forBounds: customBounds)
        return customBounds
    }
}
