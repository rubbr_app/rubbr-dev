//
//  CustomGradientLayer.swift
//  Rubbr
//
//  Created by Hamlet on 20.03.21.
//  Copyright © 2021 Hamlet Kosakyan. All rights reserved.
//

import UIKit

enum GradientType {
    case vertical
    case horizontal
    case custom(startPoint: CGPoint, endPoint: CGPoint)
    
    var startPoint: CGPoint {
        switch self {
        case .vertical: return CGPoint(x: 0.5, y: 0.0)
        case .horizontal: return CGPoint(x: 0.0, y: 0.5)
        case .custom(let startpoint, _): return startpoint
        }
    }
    
    var endPoint: CGPoint {
        switch self {
        case .vertical: return CGPoint(x: 0.5, y: 1.0)
        case .horizontal: return CGPoint(x: 1.0, y: 0.5)
        case .custom(_, let endpoint): return endpoint
        }
    }
}

class CustomGradientLayer {
    static func setGrad(to view: UIView, color1: UIColor, color2: UIColor, type: GradientType) -> UIImage {
        let gradientLayer = CAGradientLayer()
        var updatedFrame = view.bounds
        updatedFrame.size.height += 20
        gradientLayer.frame = updatedFrame
        gradientLayer.colors = [color1.cgColor, color2.cgColor]
        gradientLayer.startPoint = type.startPoint
        gradientLayer.endPoint = type.endPoint
        return image(fromLayer: gradientLayer)
    }
    
    static func image(fromLayer layer: CALayer) -> UIImage {
        UIGraphicsBeginImageContext(layer.frame.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return outputImage!
    }
}
