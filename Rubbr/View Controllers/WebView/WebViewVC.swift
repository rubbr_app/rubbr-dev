//
//  WebViewVC.swift
//  Rubbr
//
//  Created by MacOS on 3/11/19.
//  Copyright © 2019 Hamlet Kosakyan. All rights reserved.
//

import Foundation
import WebKit

class WebViewVC: BaseVC, WKNavigationDelegate {
    private lazy var webView: WKWebView = {
        let webV = WKWebView()
        webV.navigationDelegate = self
        return webV
    } ()

    override func loadView() {
        view = webView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        set(title: LEAVE_FEEDBACK)
        setMenuButton()
        startLoading()
    }
    
    private func startLoading() {
        guard let url = URL(string: kLeaveFeedbackURL) else { return }
        webView.load(URLRequest(url: url))
    }
}
