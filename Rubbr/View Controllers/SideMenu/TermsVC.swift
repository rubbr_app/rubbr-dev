//
//  TermsVC.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 27/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import UIKit

class TermsVC: BaseVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        set(title: "Terms & Conditions")
        setMenuButton()
        GoogleAnalytics.logEvent(event: kTermsAndConditionsOpened, parameters: nil)
    }
}
