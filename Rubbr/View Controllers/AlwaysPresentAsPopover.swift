//
//  AlwaysPresentAsPopover.swift
//  Rubbr
//
//  Created by Hamlet on 20.03.21.
//  Copyright © 2021 Hamlet Kosakyan. All rights reserved.
//

import UIKit

class AlwaysPresentAsPopover: NSObject, UIPopoverPresentationControllerDelegate {
    private static let sharedInstance = AlwaysPresentAsPopover()

    private override init() {
        super.init()
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }

    static func configurePresentation(forController controller: UIViewController) -> UIPopoverPresentationController {
        controller.modalPresentationStyle = .popover
        let presentationController = controller.presentationController as! UIPopoverPresentationController
        presentationController.delegate = AlwaysPresentAsPopover.sharedInstance
        return presentationController
    }
}
