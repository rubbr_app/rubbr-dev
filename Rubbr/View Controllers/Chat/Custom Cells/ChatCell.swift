//
//  ChatCell.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 24/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
}
