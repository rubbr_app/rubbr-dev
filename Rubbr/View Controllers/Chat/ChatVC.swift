//
//  ChatVC.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 24/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase
import Alamofire
import CoreLocation
import MapKit

class ChatVC: BaseVC {
    //MARK: IBoutlets
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var bottomConstriant: NSLayoutConstraint!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var navigationButton: UIButton!
    @IBOutlet weak var popView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var coveredButton: UIButton!

    //MARK: Variables
    var amount = Double()
    var name = String()
    var token = String()
    var messageArray = JSONArray()
    var location = CLLocationCoordinate2D()

    //MARK: Class Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        enableKeyboard()
        customiseUI()
        updateTableContentInset()
        retrieveMessage()
        getCurrentLocation { (success) in }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        innerView.setGradient(color1: UIColor.RubbrColor.golden.color(), color2: UIColor.RubbrColor.orange.color(), frame: confirmButton.layer.frame)
        if HomeVC.type == .driver {
            let dict: JSONDictionary = [APIKeys.kIsAvailable: false, APIKeys.kLatitude: currentLocation.latitude, APIKeys.kLongitude: currentLocation.longitude, APIKeys.kDeviceToken: AppDelegate.DEVICEID, APIKeys.kShowMultipliedDriver: false, APIKeys.kCount: (UserVM.shared.driverData[APIKeys.kCount] as? Int) ?? 0]
            UserVM.shared.addDriverData(dict: dict) { (message, error) in }
        }
    }

    //MARK: Private Methods
    private func customiseUI() {
        set(title: "Thank You!", showBack: false)
        baseView.set(radius: 21.5)
        backView.set(radius: 15.0)
        
        let image = CustomGradientLayer.setGrad(to: confirmButton, color1: UIColor.RubbrColor.golden.color(), color2: UIColor.RubbrColor.orange.color(), type: .horizontal)
        
        confirmButton.setBackgroundImage(image, for: .normal)
        navigationButton.setBackgroundImage(image, for: .normal)
        navigationButton.set(radius: navigationButton.half)
        coveredButton.setBackgroundImage(image, for: .normal)
        coveredButton.set(radius: coveredButton.half)
        confirmButton.set(radius: confirmButton.half)
        setBackground()
        navigationButton.isHidden = HomeVC.type == .user
    }

    func setBackground() {
        backImageView.image = CustomGradientLayer.setGrad(to: view, color1: UIColor.RubbrColor.purpleMain.color(), color2: UIColor.RubbrColor.pinkMain.color(), type: .vertical)
    }

    private func updateTableContentInset() {
        tableView.transform = CGAffineTransform(rotationAngle: (-.pi))
        tableView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: tableView.bounds.size.width - 10)
        tableView.transform = CGAffineTransform(scaleX: 1, y: -1)
    }

    private func updateOrdersCount() {
        let db = Firestore.firestore()
        let currentCount: Int = (UserVM.shared.driverData[APIKeys.kCount] as? Int) ?? 0
        let newCount = currentCount + 1
        db.collection(APIKeys.kDriverDetails).document(DataManager.userID!).updateData([APIKeys.kCount: newCount])
    }

    private func removeRequest() {
        guard let driverId = DataManager.userID else { return }
        Database.database().reference().child(kRequests).child(driverId).removeAllObservers()
        Database.database().reference().child(kRequests).child(driverId).removeValue()
    }

    //MARK: IBActions
    @IBAction func confirmButtonAction(_ sender: UIButton) {
        if HomeVC.type == .driver {
            Database.database().reference().child(kChat).child(DataManager.chatID!).childByAutoId().updateChildValues(["Order": "Success"])
            updateOrdersCount()
            removeRequest()
            showAlert(message: "Order Completed Successfully!", title: kSuccess) { [weak self] (action) in
                DataManager.chatID = nil
                self?.loadSideMenu()
            }
        }
        
        popView.isHidden = HomeVC.type == .driver
    }

    @IBAction func sendButtonAction(_ sender: UIButton) {
        sendMessage()
    }

    @IBAction func navigationButtonAction(_ sender: UIButton) {
        if UIApplication.shared.canOpenURL(NSURL(string: "comgooglemaps://")! as URL) {
            let alert = UIAlertController(title: "Open Maps", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Google Maps", style: .default, handler: { [weak self] (action) in
                self?.openGoogleMaps()
            }))
            alert.addAction(UIAlertAction(title: "Apple Maps", style: .default, handler: { [weak self] (action) in
                self?.openAppleMaps()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
            present(alert, animated: true, completion: nil)
        } else {
            openAppleMaps()
        }
    }

    @IBAction func coveredButtonAction(_ sender: Any) {
        makePayment()
    }

    func openGoogleMaps() {
        if let url = URL(string: "comgooglemaps://"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(location.latitude),\(location.longitude)&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
        }
    }

    func openAppleMaps() {
        let source = MKMapItem(placemark: MKPlacemark(coordinate: currentLocation))
        source.name = "Source"

        let destination = MKMapItem(placemark: MKPlacemark(coordinate: location))
        destination.name = "Destination"

        MKMapItem.openMaps(with: [source, destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }
}

//MARK: Keyboard Methods
extension ChatVC {
    func enableKeyboard() {
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            bottomConstriant.constant = keyboardHeight + 7.0
        }
    }

    @objc func keyboardWillHide(_ notification: Notification) {
        bottomConstriant.constant = 7.0
    }
}

//MARK: Textview Delegates
extension ChatVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }

    func textViewDidChange(_ textView: UITextView) {
        let numberOfGlyphs = textView.layoutManager.numberOfGlyphs
        var index = 0, numberOfLines = 0
        var lineRange = NSRange(location: NSNotFound, length: 0)

        while index < numberOfGlyphs {
            textView.layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
            index = NSMaxRange(lineRange)
            numberOfLines += 1
        }
        
        textView.isScrollEnabled = numberOfLines >= 4
       
    }
}

//MARK: Tableview Datasource
extension ChatVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = messageArray.count - indexPath.row - 1
        let cell = tableView.dequeueReusableCell(withIdentifier: ChatCell.identifier) as! ChatCell
        cell.messageLabel.text = messageArray[index][APIKeys.kMessage] as? String
        let id = messageArray[index][APIKeys.kId] as? String ?? ""
        let name = id == DataManager.userID! ? "Me" : messageArray[index][APIKeys.kName] as? String
        let timeDouble = messageArray[index][APIKeys.kDate] as? Double ?? 0.0
        let date = Date(milliseconds: timeDouble)
        cell.nameLabel.text = name
        cell.timeLabel.text = date.stringFromDate(format: .time, type: .local)
//        cell.transform = CGAffineTransform(rotationAngle: (-.pi))
        cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
        return cell
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        textView.resignFirstResponder()
    }
}

//MARK: Stripe Methods
extension ChatVC {
    func makePayment() {
        Database.database().reference().child(kChat).child(DataManager.chatID!).childByAutoId().updateChildValues(["Success": "Success"])

        StripeVM.shared.makePayment(amount: amount * 100) { [weak self] (message, error) in
            guard let error = error else {
                Database.database().reference().child(kChat).child(DataManager.chatID!).removeValue()
                DataManager.chatID = nil
                self?.showAlert(message: "Thanks for chosing Rubbr, your order completed successfully", title: kSuccess, cancelAction: { (action) in
                    self?.loadSideMenu()
                })
                return
            }
            
            self?.showErrorMessage(error: error)
        }
    }
}

extension ChatVC {
    func sendMessage() {
        guard let message = textView.text, let userId = DataManager.userID, let chatId = DataManager.chatID else { return }
        sendNotification(message: message)
        let messageDict: JSONDictionary = [APIKeys.kMessage: message, APIKeys.kDate: Date().millisecondsSince1970, APIKeys.kName: name, APIKeys.kId: userId]
        Database.database().reference().child(kChat).child(chatId).childByAutoId().updateChildValues(messageDict)
        textView.text = ""
        textViewDidChange(textView)
        tableView.reloadData()
    }

    func retrieveMessage() {
        Database.database().reference().child(kChat).child(DataManager.chatID!).observe(.childAdded) {[weak self] (snapShot) in
            if let value = snapShot.value as? JSONDictionary {
                self?.view.endEditing(true)
                if value.count == 1 {
                    switch HomeVC.type {
                    case .user:
                        if let _ = value["Order"] {
                            self?.popView.isHidden = false
                            Database.database().reference().child(kChat).child(DataManager.chatID ?? "123").removeValue()
                        }
                    case .driver:
                        Database.database().reference().child(kChat).child(DataManager.chatID ?? "123").removeValue()
                        self?.showAlert(message: "Order Completed Successfully!", title: kSuccess) { (action) in
                            DataManager.chatID = nil
                            self?.loadSideMenu()
                        }
                    }
                    return
                }
                self?.messageArray.append(value)
            }
            print(snapShot)
            self?.tableView.reloadData()
        }
    }

    func sendNotification(message: String) {
        let title = "New Message"
        let body = message
        var headers: HTTPHeaders = HTTPHeaders()

        headers = ["Content-Type": "application/json", "Authorization": "key=\(SERVERKEY)"]
        let notification = ["to": token, "notification": ["body": body, "title": title, "badge": 1, "sound": "default"]] as [String: Any]

        AF.request(NOTIFICATION_URL as URLConvertible, method: .post, parameters: notification, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response)
        }
    }
}
