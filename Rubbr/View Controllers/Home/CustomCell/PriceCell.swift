//
//  PriceCell.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 19/11/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import UIKit

class PriceCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var amountButton: UIButton!
}
