//
//  DriverHomeExtension.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 24/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import Foundation
import FirebaseDatabase
import Firebase
import CoreLocation

//MARK: Firebase Driver Request Delegates
extension HomeVC {
    func getDriver() {
        UserVM.shared.getDriverData { [weak self] (message, error) in
            guard let self = self, error == nil, let available = UserVM.shared.driverData[APIKeys.kIsAvailable] as? Bool else { return }
            self.switchButton.setOn(available, animated: true)
            self.availableTitleLabel.text = available ? kAvailableTitle : kUnAvailableTitle
            self.availableDescriptionLabel.text = available ? kAvailableDescription : kUnAvailableDescription
            DataManager.availability = available
            DataManager.showMultipliedDriver = UserVM.shared.driverData[APIKeys.kShowMultipliedDriver] as? Bool
        }
    }

    func updateDriver() {
        let dict: JSONDictionary = [APIKeys.kIsAvailable: DataManager.availability ?? false, APIKeys.kLatitude: currentLocation.latitude, APIKeys.kLongitude: currentLocation.longitude, APIKeys.kDeviceToken: AppDelegate.DEVICEID, APIKeys.kShowMultipliedDriver: DataManager.showMultipliedDriver ?? false, APIKeys.kCount: (UserVM.shared.driverData[APIKeys.kCount] as? Int) ?? 0]
        UserVM.shared.addDriverData(dict: dict) { [weak self] (message, error) in
            guard let self = self else { return }
            if error != nil {
                DataManager.availability = !self.switchButton.isOn
                self.switchButton.isOn = !self.switchButton.isOn
                self.showErrorMessage(error: error! as NSError)
                return
            }
            self.getDriver()
        }
    }

    func listenUserRequests() {
        guard let driverId = DataManager.userID else { return }
        Database.database().reference().child(kRequests).child(driverId).observe(.value) { (snapShot) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self, let dict = snapShot.value as? JSONDictionary, let isCancelled = dict[APIKeys.kIsCancelled] as? Bool else { return }
                    
                if isCancelled {
                    self.acceptView.isHidden = isCancelled
                    self.driverView.isHidden = !isCancelled
                    DataManager.isDelivering = !isCancelled
                    Database.database().reference().child(kRequests).child(DataManager.userID!).removeValue()
                    return
                }

                if DataManager.isDelivering {
                    return
                }

                print("User Details: \n\(dict)")
                self.acceptView.isHidden = false
                self.driverView.isHidden = true
                    
                let name = dict[APIKeys.kNameOnRing] as? String
                let address = dict[APIKeys.kAddress] as? String
                    
                self.userId = dict[APIKeys.kId] as? String
                self.amount = dict[APIKeys.kAmount] as? Double
                DataManager.token = dict[APIKeys.kDeviceToken] as? String
                    
                let coordinate = CLLocationCoordinate2D(latitude: dict[APIKeys.kLatitude] as? Double ?? 0.0, longitude: dict[APIKeys.kLongitude] as? Double ?? 0.0)
                let distance = CLLocation(latitude: self.currentLocation.latitude, longitude: self.currentLocation.longitude).distance(from: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude))
                    
                let timeSeconds = distance / 60
                let timeMinutes = timeSeconds / 60

                self.destinationLocation = coordinate
                self.acceptNameLabel.text = name
                self.acceptAddressLabel.text = address
                self.acceptView.isUserInteractionEnabled = true
                self.acceptTimeLabel.text = "\(Int(timeMinutes)) minutes"

                DataManager.isDelivering = true
            }
        }
    }

    func sendAcceptRequest() {
        guard let driverId = DataManager.userID else { return }
        Database.database().reference().child(kRequests).child(driverId).updateChildValues([APIKeys.kLatitude: destinationLocation.latitude, APIKeys.kLongitude: destinationLocation.longitude, APIKeys.kIsAccepted: true, APIKeys.kIsCancelled: false])

        guard let chatvc = storyboard?.instantiateViewController(withIdentifier: CHAT_VC) as? ChatVC else { return }
        DataManager.name = "Driver"
        DataManager.amount = amount
        chatvc.name = "Driver"
        chatvc.amount = amount ?? 5
        chatvc.token = DataManager.token!
        chatvc.location = destinationLocation
        DataManager.chatID = DataManager.userID!
        navigationController?.show(chatvc, sender: self)
    }

    func sendLocationUpdates() {
        let messageDict: JSONDictionary = [APIKeys.kLatitude: currentLocation.latitude, APIKeys.kLongitude: currentLocation.longitude]
        guard let userID = DataManager.userID else { return }
        Database.database().reference().child("Drivers").child(userID).childByAutoId().updateChildValues(messageDict)
    }
}
