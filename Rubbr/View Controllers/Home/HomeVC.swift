//
//  HomeVC.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 19/11/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import UIKit
import MapKit
import SwiftSpinner
import Firebase
import Alamofire
import FirebaseDatabase
import CoreLocation

enum ScreenType: String {
    case user = "user"
    case driver = "driver"
}

protocol SendRequestDelegate: class {
    func sendRequest(driver: Driver?, amount: Double?)
}

class HomeVC: BaseVC, CLLocationManagerDelegate {
    //MARK: IBoutlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var driverView: UIView!
    @IBOutlet weak var availableTitleLabel: UILabel!
    @IBOutlet weak var switchButton: UISwitch!
    @IBOutlet weak var availableDescriptionLabel: UILabel!
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var acceptView: UIView!
    @IBOutlet weak var acceptNameLabel: UILabel!
    @IBOutlet weak var acceptAddressLabel: UILabel!
    @IBOutlet weak var acceptTimeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!

    static var type: ScreenType = .user
    
    //MARK: Variables
    var annotations = [CLLocationCoordinate2D]()
    var myAnnotations = [CLLocationCoordinate2D]()
    var destinationLocation = CLLocationCoordinate2D()
    
    var driverArray = [Driver]()
    
    var userId: String? = nil
    var amount: Double? = nil
    var requestAccepted = false
    var pullUpController: PriceVC?
    
    lazy var locationManager: CLLocationManager = {
        var lManager = CLLocationManager()
        lManager.delegate = self;
        lManager.distanceFilter = 100;
        lManager.desiredAccuracy = kCLLocationAccuracyBest;
        lManager.requestAlwaysAuthorization()
        lManager.requestWhenInUseAuthorization()
        lManager.startUpdatingLocation()
        return lManager
    }()

    
    private func makeSearchViewControllerIfNeeded() -> PriceVC {
        let currentPullUpController = children
            .filter({ $0 is PriceVC })
            .first as? PriceVC
        let pullUpController: PriceVC = currentPullUpController ?? UIStoryboard(storyboard: .Home).instantiateViewController(withIdentifier: PRICE_VC) as! PriceVC
        pullUpController.delegate = self
        pullUpController.driverArray = driverArray
        return pullUpController
    }

    //MARk: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        DataManager.isDelivering = false
        DataManager.isOnboardingDone = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        mapView.delegate = self
        customiseUI()
        setCurrentLocation()
        NotificationCenter.default.addObserver(self, selector: #selector(self.didGetToken), name: .didGetToken, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.didGetNotification), name: .didGetNotification, object: nil)
        if HomeVC.type == .driver {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(titleButtonAction))
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: .didGetToken, object: nil)
        NotificationCenter.default.removeObserver(self, name: .didGetNotification, object: nil)

        if HomeVC.type == .user {
            for driver in driverArray {
                Database.database().reference().child("Drivers").child(driver.id).removeAllObservers()
            }
        }
    }

    //MARK: Notifications Methods
    @objc private func didGetToken() {
        if HomeVC.type == .user {
            getAllDrivers()
        } else {
            driverView.isHidden = false
            updateDriver()
            listenUserRequests()
        }
    }

    @objc private func didGetNotification() {
        listenUserRequests()
    }

    //MARK: IBActions
    @IBAction func okayButtonAction(_ sender: UIButton) {
        userView.isHidden = true
    }

    @IBAction func switchButtonAction(_ sender: UISwitch) {
        if !sender.isOn, let driverId = DataManager.userID {
            Database.database().reference().child("Drivers").child(driverId).removeAllObservers()
            Database.database().reference().child("Drivers").child(driverId).removeValue()
        }

        availableTitleLabel.text = sender.isOn ? kAvailableTitle : kUnAvailableTitle
        availableDescriptionLabel.text = sender.isOn ? kAvailableDescription : kUnAvailableDescription
        DataManager.availability = sender.isOn
        updateDriver()
    }

    @IBAction func acceptButtonAction(_ sender: UIButton) {
        acceptView.isHidden = true
        driverView.isHidden = false
        acceptView.isUserInteractionEnabled = false
        sendAcceptRequest()
    }

    @objc func titleButtonAction() {
        DataManager.showMultipliedDriver = !(DataManager.showMultipliedDriver ?? false)
        let messageSuffix = (DataManager.showMultipliedDriver ?? false) ? "enabled." : "disabled."
        showAlert(message: "Multiply Driver Feature is " + messageSuffix)
        updateDriver()
    }

    //MARK: Private Methods
    private func customiseUI() {
        switchButton.set(radius: switchButton.half)
        acceptView.set(radius: 10.0)

        //Set Navigations
        set(title: "")
        setNavigationImage()
        setMenuButton()
    }

    //MARK: Algorithm of drivers
    func handleAlgorithm() {
        myAnnotations = [CLLocationCoordinate2D]()
        for driver in UserVM.shared.driverArray {
            guard let latitude = (driver.data()[APIKeys.kLatitude] as? Double), let longitude = (driver.data()[APIKeys.kLongitude] as? Double) else { continue }
            let driverLocation = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let distance = CLLocation(latitude: currentLocation.latitude, longitude: currentLocation.longitude).distance(from: CLLocation(latitude: driverLocation.latitude, longitude: driverLocation.longitude))
            let deviceToken = (driver.data()[APIKeys.kDeviceToken] as? String) ?? ""
            if distance <= 3000 && ((driver.data()[APIKeys.kIsAvailable] as? Bool) ?? false) {
                if (driver.data()[APIKeys.kShowMultipliedDriver] as? Bool) ?? false {
                    let latitude = driver.data()[APIKeys.kLatitude] as! Double
                    let longitude = driver.data()[APIKeys.kLongitude] as! Double

                    let testDriver1Location = CLLocationCoordinate2D(latitude: latitude + 0.017, longitude: longitude + 0.017)
                    let testDriver2Location = CLLocationCoordinate2D(latitude: latitude - 0.01, longitude: longitude - 0.01)

                    driverArray.append(Driver(location: testDriver1Location, distance: distance / 1000, id: driver.documentID, token: deviceToken))
                    driverArray.append(Driver(location: testDriver2Location, distance: distance / 1000, id: driver.documentID, token: deviceToken))
                    driverArray.append(Driver(location: driverLocation, distance: distance / 1000, id: driver.documentID, token: deviceToken))
                }

                if (driverArray.filter { $0.id == driver.documentID }).count == 0 {
                    driverArray.append(Driver(location: driverLocation, distance: distance / 1000, id: driver.documentID, token: deviceToken))
                }
            }
        }

        prepareUserView()
        listenLocationUpdatesForDrivers()
    }

    func prepareUserView () {
        if driverArray.count == 0 {
            statusLabel.text = kNoDriversAvailable
            userView.isHidden = false
        } else {
            driverArray.sort(by: { $0.distance < $1.distance })
            addPullUpController()
            addAnnotations()
        }
    }

    func prepareUserViewForSupportedLocations () {
        var isInSupportedLocation = false
        for region in SUPPORTED_REGIONS {
            isInSupportedLocation = region.contains(currentLocation)
        }
        
        if isInSupportedLocation {
            if driverArray.count == 0 {
                statusLabel.text = kNoDriversAvailable
                userView.isHidden = false
            } else {
                driverArray.sort(by: { $0.distance < $1.distance })
                addPullUpController()
                addAnnotations()
            }
        } else {
            statusLabel.text = kUnsupportedLocation
            userView.isHidden = false
            LocationManager.shared.getAddress(location: currentLocation) { [weak self] (address, placemark) in
                if let subLocality = placemark.subLocality, let self = self {
                    let currentLocation = self.currentLocation
                    GoogleAnalytics.logEvent(event: kOutOfRegions, parameters: ["location": "\(subLocality)", "lat": "\(currentLocation.latitude)", "long": "\(currentLocation.longitude)"])
                }
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        setCurrentLocation()
        locationManager.startUpdatingLocation()
    }
}

//Pull up controllers
extension HomeVC {
    private func addPullUpController() {
        if pullUpController != nil {
            return
        }
        pullUpController = makeSearchViewControllerIfNeeded()
        _ = pullUpController!.view // call pullUpController.viewDidLoad()

        addPullUpController(pullUpController!,
                            initialStickyPointOffset: pullUpController!.initialPointOffset,
                            animated: true)
    }
}

//MARK: Handle Map Methods
extension HomeVC: MKMapViewDelegate {
    private func setCurrentLocation() {
        DispatchQueue.main.async { [weak self] in
            self?.getCurrentLocation { [weak self] (success) in
                guard let self = self else { return }
                if success {
//                    let circle = MKCircle(center: self.currentLocation, radius: 500)
//                    self.mapView.addOverlay(circle)
                    let viewRegion = MKCoordinateRegion(center: self.currentLocation, latitudinalMeters: 1200, longitudinalMeters: 1200)
                    self.mapView.setRegion(viewRegion, animated: true)

                    //Handle User Type
                    if HomeVC.type == .user {
                        self.getAllDrivers()
                    } else {
                        self.driverView.isHidden = false
                        self.updateDriver()
                        self.sendLocationUpdates()
                        self.listenUserRequests()
                    }
                }
            }
        }
    }

    private func addAnnotations() {
        for driver in driverArray {
            let annotation = MKPointAnnotation()
            annotation.coordinate = driver.location
            mapView.addAnnotation(annotation)
        }
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            let pin = mapView.view(for: annotation) ?? MKAnnotationView(annotation: annotation, reuseIdentifier: nil)
            pin.image = #imageLiteral(resourceName: "myMarker")
            return pin
        }
        let pin = mapView.view(for: annotation) ?? MKAnnotationView(annotation: annotation, reuseIdentifier: nil)
        pin.image = #imageLiteral(resourceName: "driverMarker")
        return pin
    }

//    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
//        let renderer = MKCircleRenderer(overlay: overlay)
//        renderer.fillColor = UIColor.RubbrColor.purpleMain.color(alpha: 0.7)
//        renderer.strokeColor = UIColor.RubbrColor.purpleMain.color(alpha: 0.3)
//        renderer.lineWidth = 8
//        return renderer
//    }
}
