//
//  SideMenuVC.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 01/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import UIKit
import MessageUI

enum SideMenuType {
    case withoutLogin
    case withLogin
    case driver
}

class SideMenuVC: BaseVC {
    //MARK: IBOUtlets
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var logoutView: UIView!
    
    //MARK: Variables
    var type: SideMenuType = .withoutLogin
    var userLoginArray = USER_LOGIN_ARRAY
    var driverArray = DRIVER_ARAY
    var myArray = [String]()
    var imageArray = [UIImage]()

    var userImageArray = [#imageLiteral(resourceName: "tellFriendIcon"), #imageLiteral(resourceName: "addressIcon"), #imageLiteral(resourceName: "cardIcon"), #imageLiteral(resourceName: "termsOfServiceIcon"), #imageLiteral(resourceName: "contactBuble"), #imageLiteral(resourceName: "feedabckIcon")]
    var driverImageArray = [#imageLiteral(resourceName: "tellFriendIcon"), #imageLiteral(resourceName: "termsOfServiceIcon"), #imageLiteral(resourceName: "contactBuble"), #imageLiteral(resourceName: "feedabckIcon")]

    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundImageView.image = CustomGradientLayer.setGrad(to: view, color1: UIColor.RubbrColor.purpleMain.color(), color2: UIColor.RubbrColor.pinkMain.color(), type: .vertical)

        logoutView.isHidden = DataManager.userID == nil

        switch HomeVC.type {
        case .user:
            myArray = DataManager.userID != nil ? userLoginArray : driverArray
            imageArray = DataManager.userID != nil ? userImageArray : driverImageArray
        case .driver:
            driverArray.removeLast()
            myArray = driverArray
            imageArray = driverImageArray
        }
    }

    @IBAction func logoutButtonAction(_ sender: UIButton) {
        showAlert(message: kLogoutMessage, title: nil, otherButtons: [YES: { [weak self] (action) in
            self?.appDelegate?.sideMenu?.hideMenu()
            self?.loGout()
            GoogleAnalytics.logEvent(event: kLoggedOut, parameters: nil)
        }], cancelTitle: NO) { [weak self] (action) in
            self?.appDelegate?.sideMenu?.hideMenu()
        }
    }

    @IBAction func homeButtonAction(_ sender: UIButton) {
        let homevc = storyboard?.instantiateViewController(withIdentifier: HOME_VC) as! HomeVC
        let navvc = UINavigationController(rootViewController: homevc)
        appDelegate?.sideMenu?.setContentViewController(to: navvc)
        appDelegate?.sideMenu?.hideMenu()
    }
}

extension SideMenuVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuCell.identifier) as! SideMenuCell
        cell.titleLabel.text = myArray[indexPath.row]
        cell.iconImageView.image = imageArray[indexPath.row]
        return cell
    }
}

extension SideMenuVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        appDelegate?.sideMenu?.hideMenu()
        
        switch indexPath.row {
        case 0:
            shareContent()
        case 1:
            if DataManager.userID != nil && HomeVC.type == .user {
                UserLoginVC.type = .address
                loginScreen()
                return
            }
            termsScreen()
        case 2:
            if DataManager.userID != nil && HomeVC.type == .user {
                UserLoginVC.type = .payment
                loginScreen()
                return
            }
            sendEmail()
        case 3:
            if HomeVC.type == .user {
                DataManager.userID != nil ? termsScreen() : webView()
                return
            }
            sendEmail()
        case 4:
            if HomeVC.type == .user {
                DataManager.userID != nil ? sendEmail() : webView()
                return
            }
        case 5:
            webView()
        default:
            sendEmail()
        }
    }
}

extension SideMenuVC: MFMailComposeViewControllerDelegate {
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([EMAIL_ADDRESS])
            mail.setMessageBody(EMAIL_CONTENT, isHTML: true)
            present(mail, animated: true)
        }
        GoogleAnalytics.logEvent(event: kContactUs, parameters: nil)
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

// MARK:- Private Methods
private extension SideMenuVC {
    func shareContent() {
        let activityViewController = UIActivityViewController(activityItems: [JOIN_RUUBR_NOW], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = view
        present(activityViewController, animated: true, completion: nil)
        GoogleAnalytics.logEvent(event: kTellAFriend, parameters: nil)
    }
    
    func loginScreen() {
        guard let nvc = UIStoryboard(storyboard: .Main).instantiateViewController(withIdentifier: USER_LOGIN_VC) as? UserLoginVC else { return }
        let navvc = UINavigationController(rootViewController: nvc)
        nvc.amount = 5
        nvc.isFromSide = true
        appDelegate?.sideMenu?.setContentViewController(to: navvc)
    }
    
    func termsScreen() {
        GoogleAnalytics.logEvent(event: kTermsAndConditionsOpened, parameters: nil)
        guard let termsvc = storyboard?.instantiateViewController(withIdentifier: TERMS_VC) as? TermsVC else { return }
        let navvc = UINavigationController(rootViewController: termsvc)
        appDelegate?.sideMenu?.setContentViewController(to: navvc)
    }
    
    func webView() {
        GoogleAnalytics.logEvent(event: kLeaveFeedbackTapped, parameters: nil)
        guard let webViewvc = UIStoryboard(storyboard: .Main).instantiateViewController(withIdentifier: "WebViewVC") as? WebViewVC else { return }
        let navvc = UINavigationController(rootViewController: webViewvc)
        appDelegate?.sideMenu?.setContentViewController(to: navvc)
    }
    
    func loGout() {
        UserVM.shared.logout {[weak self] (messsage, error) in
            self?.logout()
        }
    }
}
