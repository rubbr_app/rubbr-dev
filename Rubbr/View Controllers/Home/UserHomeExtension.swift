//
//  UserHomeExtension.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 24/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import Firebase
import SwiftSpinner
import Alamofire
import FirebaseDatabase
import CoreLocation

//MARK: User Requests
extension HomeVC: SendRequestDelegate {
    func getAllDrivers() {
        UserVM.shared.getAllDrivers { [weak self] (message, error) in
            self?.handleAlgorithm()
        }
    }

    func sendRequest(driver: Driver?, amount: Double?) {
        if let driver = driver {
            SwiftSpinner.show("Searching Driver")
            sendNotification(driver: driver)
            sendRequestDriver(driver: driver, amount: amount ?? 0.0)
            listenRequest(driver: driver, amount: amount)

            let date = Date()
            let calendar = Calendar.current
            GoogleAnalytics.logEvent(event: kRubbrOrdered, parameters: ["time": "\(calendar.component(.hour, from: date)):\(calendar.component(.minute, from: date))"])
        }
    }

    func sendRequestDriver(driver: Driver, amount: Double!) {
        Database.database().reference().child(kRequests).child(driver.id).setValue([APIKeys.kLatitude: currentLocation.latitude, APIKeys.kLongitude: currentLocation.longitude, APIKeys.kNameOnRing: DataManager.userName!, APIKeys.kAddress: DataManager.userAddress!, APIKeys.kId: DataManager.userID!, APIKeys.kAmount: amount ?? 0.0, APIKeys.kDeviceToken: AppDelegate.DEVICEID, APIKeys.kIsAccepted: false, APIKeys.kIsCancelled: false])

        DispatchQueue.main.asyncAfter(deadline: .now() + 120) { [weak self] in
            guard let self = self else { return }
            if !self.requestAccepted {
                Database.database().reference().child(kRequests).child(driver.id).updateChildValues([APIKeys.kLatitude: self.currentLocation.latitude, APIKeys.kLongitude: self.currentLocation.longitude, APIKeys.kNameOnRing: DataManager.userName!, APIKeys.kAddress: DataManager.userAddress!, APIKeys.kId: DataManager.userID!, APIKeys.kAmount: amount ?? 0.0, APIKeys.kDeviceToken: AppDelegate.DEVICEID, APIKeys.kIsAccepted: false, APIKeys.kIsCancelled: true])
            }
            SwiftSpinner.hide()
        }
    }

    func listenRequest(driver: Driver, amount: Double!) {
        Database.database().reference().child(kRequests).child(driver.id).observe(.value) { (snapShot) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self, let dict = snapShot.value as? JSONDictionary, let isAccepted = dict[APIKeys.kIsAccepted] as? Bool, isAccepted else { return }
                
                SwiftSpinner.hide()
                self.requestAccepted = true
                let chatvc = self.storyboard?.instantiateViewController(withIdentifier: CHAT_VC) as! ChatVC
                DataManager.name = DataManager.userName!
                DataManager.amount = amount
                DataManager.token = driver.token
                chatvc.name = DataManager.userName!
                chatvc.amount = amount
                chatvc.token = driver.token
                DataManager.chatID = driver.id
                self.navigationController?.show(chatvc, sender: self)

                Database.database().reference().child(kRequests).child(DataManager.userID!).removeValue()

                print(snapShot)
            }
        }
    }

    func sendNotification(driver: Driver) {
        let title = "New Request"
        let body = "You have received a new delivery request"
        var headers: HTTPHeaders = HTTPHeaders()

        headers = ["Content-Type": "application/json", "Authorization": "key=\(SERVERKEY)"]
        print(driver.token ?? "")
        let notification = [
            "to": (driver.token ?? ""),
            "notification": ["body": body, "title": title, "badge": 1, "sound": "default"]] as [String: Any]

        AF.request(NOTIFICATION_URL as URLConvertible, method: .post, parameters: notification, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response)
        }
    }

    func listenLocationUpdatesForDrivers() {
        for driver in driverArray {
            Database.database().reference().child("Drivers").child(driver.id).observe(.childAdded) { (snapShot) in
                DispatchQueue.main.async { [weak self] in
                    let indexOfDriver = self?.driverArray.firstIndex { $0.id == driver.id }
                    
                    guard let self = self, let dict = snapShot.value as? JSONDictionary, let index = indexOfDriver, let latitude = dict[APIKeys.kLatitude] as? Double, let longitude = dict[APIKeys.kLongitude] as? Double else { return }

                    self.mapView.removeAnnotations(self.mapView.annotations)
                    var newDriver = Driver()
                    newDriver.distance = driver.distance
                    newDriver.id = driver.id
                    newDriver.location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                    newDriver.token = driver.token

                    self.driverArray.remove(at: index)
                    self.driverArray.insert(newDriver, at: index)
                    self.prepareUserView()
                    print("User Details: \n\(dict)")
                }
            }
        }
    }
}
