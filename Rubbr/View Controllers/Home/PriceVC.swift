//
//  PriceVC.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 19/11/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import UIKit
import PullUpController
import Firebase

class PriceVC: PullUpController {
    enum InitialState {
        case contracted
        case expanded
    }

    @IBOutlet weak var tableView: UITableView!
    
    //MARK: Variables
    var driverArray = [Driver]()
    var priceArray: [Double] = [8, 7, 6, 5]
    var titleArray = ["Nearby", "Close Enough", "Kinda Far", "Far"]
    var index = Int()
    var initialState: InitialState = .contracted
    var initialPointOffset: CGFloat {
        switch initialState {
        case .contracted:
            return 100
        case .expanded:
            return 450
        }
    }
    
    public var portraitSize: CGSize = .zero
    weak var delegate: SendRequestDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        portraitSize = CGSize(width: min(UIScreen.main.bounds.width, UIScreen.main.bounds.height), height: 450)
        tableView.attach(to: self)
        tableView.tableFooterView = UIView()
    }

    override var pullUpControllerPreferredSize: CGSize {
        return portraitSize
    }

    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
        switch initialState {
        case .contracted:
            return [100]
        case .expanded:
            return [100]
        }
    }

    var pullUpControllerIsBouncingEnabled: Bool {
        return false
    }

    func pullUpControllerAnimate(withDuration duration: TimeInterval,
                                 animations: @escaping () -> Void,
                                 completion: ((Bool) -> Void)?) {
        UIView.animate(withDuration: 0.3,
                       delay: 0,
                       usingSpringWithDamping: 0.7,
                       initialSpringVelocity: 0,
                       options: .curveEaseInOut,
                       animations: animations,
                       completion: completion)
    }
}

extension PriceVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return driverArray.count > 6 ? 6 : driverArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PriceCell.identifier) as! PriceCell
        cell.amountButton.addTarget(self, action: #selector(self.priceButtonAction(sender:)), for: .touchUpInside)
        cell.amountButton.tag = indexPath.row
        cell.titleLabel.text = indexPath.row <= 3 ? titleArray[indexPath.row] : "Far"
        cell.amountButton.setTitle(indexPath.row <= 3 ? "\(priceArray[indexPath.row])€" : "5€", for: .normal)
        return cell
    }

    @objc func priceButtonAction(sender: UIButton) {
        let storyboard = UIStoryboard(storyboard: .Main)
        let nvc = storyboard.instantiateViewController(withIdentifier: USER_LOGIN_VC) as! UserLoginVC
        index = sender.tag
        let amount = index <= 3 ? priceArray[index] : 5
        nvc.amount = amount
        nvc.delegate = self
        nvc.isFromSide = false
        navigationController?.show(nvc, sender: self)
        GoogleAnalytics.logEvent(event: kPriceSelected, parameters: ["priceIs": "\(amount)"])
    }
}

extension PriceVC: SendRequestDelegate {
    func sendRequest(driver: Driver?, amount: Double?) {
        let amount = index <= 3 ? priceArray[index] : 5
        delegate?.sendRequest(driver: driverArray[index], amount: amount)
    }
}
