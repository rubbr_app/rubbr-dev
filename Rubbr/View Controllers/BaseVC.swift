//
//  BaseVC.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 10/11/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import UIKit
import CoreLocation
import SideMenuSwift
import Firebase
import FirebaseDatabase

class BaseVC: UIViewController {
    var currentLocation = CLLocationCoordinate2D()
    var appDelegate: AppDelegate? {
        return (UIApplication.shared.delegate as? AppDelegate)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handleLocation()
    }

    func logout() {
        if HomeVC.type == .driver {
            logOutFromDriver()
            return
        }
        
        DataManager.rmeoveOldValues()
        DataManager.userType = ScreenType.user.rawValue
        HomeVC.type = .user
        loadSideMenu()
    }

    func logOutFromDriver() {
        if let driverId = DataManager.userID {
            Database.database().reference().child(DRIVERS).child(driverId).removeAllObservers()
            Database.database().reference().child(DRIVERS).child(driverId).removeValue()
        }
        
        var isDone = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
            if !isDone {
                Indicator.shared.hideIndicator()
                self?.navigateToOnboarding()
            }
        }
        
        let dict: JSONDictionary = [APIKeys.kIsAvailable: false, APIKeys.kLatitude: currentLocation.latitude, APIKeys.kLongitude: currentLocation.longitude, APIKeys.kDeviceToken: AppDelegate.DEVICEID, APIKeys.kShowMultipliedDriver: false, APIKeys.kCount: (UserVM.shared.driverData[APIKeys.kCount] as? Int) ?? 0]
        
        UserVM.shared.addDriverData(dict: dict) { (message, error) in
            DispatchQueue.main.async { [weak self] in
                isDone = true
                self?.navigateToOnboarding()
            }
        }
    }
    
    func navigateToOnboarding() {
        DataManager.rmeoveOldValues()
        guard let vc = UIStoryboard(storyboard: .Main).instantiateViewController(withIdentifier: ONBOARDING_VC) as? OnboardingVC else { return }
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.layer.add(CATransition.transition, forKey: kCATransition)
        window.rootViewController = UINavigationController(rootViewController: vc)
        window.makeKeyAndVisible()
    }

    func handleLocation() {
        self.getCurrentLocation { [weak self] (success) in
            if success { return }
            
            self?.showAlert(message: ENABLE_LOCATION_FROM_SETTINGS, cancelTitle: OKAY, cancelAction: { [weak self] (action) in
                self?.openSettings()
            })
        }
    }
}

//MARK: Location Handling
extension BaseVC {
    func getCurrentLocation(success: @escaping (Bool) -> ()) {
        LocationManager.shared.askPermissionsAndFetchLocationWithCompletion { [weak self] (location, placemark, error) in
            if let _ = error {
                self?.showAlert(message: kLocationMessage, cancelAction: { (action) in
                    self?.openSettings()
                })
                success(false)
                return
            }
            guard let location = location else { return }
            self?.currentLocation = location.coordinate
            LocationManager.shared.getAddress(location: location.coordinate, completion: { (address, placemark) in
                if let subLocality = placemark.subLocality {
                    GoogleAnalytics.logEvent(event: kLocationDetected, parameters: ["location": "\(subLocality)", "lat": "\(location.coordinate.latitude)", "long": "\(location.coordinate.longitude)"])
                }
            })
            success(true)
        }
    }
}

//MARK: Navigation Methods
extension BaseVC {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func hideNavigationBar() {
        navigationController?.isNavigationBarHidden = true
    }

    //Set Navigation Title
    func set(title: String, showBack: Bool = true) {
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.isTranslucent = false

        navigationController?.navigationBar.setBackgroundImage(CustomGradientLayer.setGrad(to: navigationController?.navigationBar ?? UIView(), color1: UIColor.RubbrColor.purpleMain.color(), color2: UIColor.RubbrColor.pinkMain.color(), type: .custom(startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5))), for: .any, barMetrics: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.hidesBackButton = !showBack
        
        let titleButton = UIButton(frame: CGRect(x: 50, y: 0, width: 110, height: 30))
        titleButton.titleLabel?.textAlignment = .center
        titleButton.setTitle(title, for: .normal)
        titleButton.isUserInteractionEnabled = false
        titleButton.titleEdgeInsets = UIEdgeInsets(top: 3, left: 0, bottom: 7, right: 0)
        titleButton.setTitleColor(UIColor.white, for: .normal)
        titleButton.titleLabel?.font = UIFont.MyFont.medium.fontWithSize(size: 14.0)
        navigationItem.titleView = titleButton
        
        if showBack {
            setBackButton()
        }
    }

    func setNavigationImage() {
        let titleButton = UIButton(frame: CGRect(x: 50, y: 0, width: 200, height: 25))
        titleButton.titleLabel?.textAlignment = .center
        titleButton.imageView?.contentMode = .scaleAspectFit
        titleButton.setTitle(title, for: .normal)
        titleButton.clipsToBounds = true
        titleButton.isUserInteractionEnabled = false
        titleButton.setImage(#imageLiteral(resourceName: "rubbrNavigation"), for: .normal)
        titleButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 15, right: 0)
        navigationItem.titleView = titleButton
    }

    //MARK: Back Button
    func setBackButton() {
        let backButton = UIButton()
        backButton.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        backButton.setImage(#imageLiteral(resourceName: "backArrow"), for: .normal)
        backButton.imageView?.contentMode = .scaleAspectFit
        backButton.addTarget(self, action: #selector(self.backButtonAction), for: .touchUpInside)
        backButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: -30, bottom: 0, right: 0)
        let leftBarButton = UIBarButtonItem()
        leftBarButton.customView = backButton
        navigationItem.leftBarButtonItem = leftBarButton
    }

    @objc func backButtonAction() {
        view.endEditing(true)
        let backDone = navigationController?.popViewController(animated: true)
        if backDone == nil {
            navigationController?.dismiss(animated: true, completion: nil)
        }
    }

    //MARK: Side Menu
    func setMenuButton() {
        let menuButton = UIButton()
        menuButton.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        menuButton.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
        menuButton.addTarget(self, action: #selector(self.menuButtonAction), for: .touchUpInside)
        menuButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: -30, bottom: 0, right: 0)
        let leftBarButton = UIBarButtonItem()
        leftBarButton.customView = menuButton
        navigationItem.leftBarButtonItem = leftBarButton
    }
}

//MARK: Side Menu Methods
extension BaseVC {
    func loadSideMenu() {
        appDelegate?.loadLeftSideMenu()
    }

    @objc func menuButtonAction() {
        view.endEditing(true)
        appDelegate?.sideMenu?.revealMenu()
    }
}

//MARK: Alert Methods
extension BaseVC {
    func showAlert(message: String?, title: String? = INFORMATION, otherButtons: [String: ((UIAlertAction) -> ())]? = nil, cancelTitle: String = OKAY, cancelAction: ((UIAlertAction) -> ())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelAction))

        if let otherButtons = otherButtons {
            for key in otherButtons.keys {
                alert.addAction(UIAlertAction(title: key, style: .default, handler: otherButtons[key]))
            }
        }
        present(alert, animated: true, completion: nil)
    }

    func showErrorMessage(error: Error?) {
        if let error = error {
            let alert = UIAlertController(title: ERROR, message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: OKAY, style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        
        let alert = UIAlertController(title: INFORMATION, message: UNABLE_TO_PROCEED_YOUR_REQUEST, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: OKAY, style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }

    func openSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }

        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)")
            })
        }
    }
}
