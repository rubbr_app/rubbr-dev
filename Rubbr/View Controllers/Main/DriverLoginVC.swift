//
//  DriverLoginVC.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 01/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import UIKit
import Firebase

class DriverLoginVC: BaseVC {
    //MARK: IBOutlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var firstTimeView: UIView!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var availableButton: UIButton!
    @IBOutlet weak var checkLaterButton: UIButton!

    //MARK: Variables
    var availability = false
    var showMultipliedDriver = false

    //MARK: Class Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customiseUI()
    }

    //MARK: Private Methods
    private func customiseUI() {
        firstTimeView.isHidden = true
        baseView.set(radius: 10.0)
        loginButton.set(radius: 25.0)
        emailTextField.set(radius: 20.0)
        passwordTextField.set(radius: 20.0)
        
        let image = CustomGradientLayer.setGrad(to: view, color1: UIColor.RubbrColor.golden.color(), color2: UIColor.RubbrColor.orange.color(), type: .vertical)
        
        loginButton.setBackgroundImage(image, for: .normal)
        availableButton.setBackgroundImage(image, for: .normal)
        availableButton.set(radius: 21.5)
        checkLaterButton.set(radius: 21.5, borderColor: UIColor.lightGray, borderWidth: 2)
    }

    //MARK: IBActions
    @IBAction func loginButtonAction(_ sender: UIButton) {
        if let message = validateData() {
            showAlert(message: message)
            return
        }
        loginWithEmail()
    }

    @IBAction func forgotButtonAction(_ sender: UIButton) {
    }

    @IBAction func dismissButtonAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func availabilityAction(_ sender: UIButton) {
        availability = sender.tag == 10
        saveUserData()
    }
}

//MARK: Fiebase Authentication
extension DriverLoginVC {
    func validateData() -> String? {
        if !emailTextField.isValidEmail {
            return kEmailValidation
        }
        
        if !passwordTextField.isValidPassword {
            return kPasswordValidation
        }
        
        return nil
    }

    func loginWithEmail() {
        UserVM.shared.loginWith(email: emailTextField.text!, password: passwordTextField.text!) { [weak self] (message, error) in
            if error != nil {
                self?.showErrorMessage(error: error! as NSError)
                return
            }
            
            DataManager.userType = ScreenType.driver.rawValue
            self?.getUserData()
        }
    }

    func getUserData() {
        UserVM.shared.getDriverData(response: { [weak self] (message, error) in
            if error == nil && message == nil {
                self?.firstTimeView.isHidden = false
                return
            }
                
            self?.updateDriver()
        })
    }

    func updateDriver() {
        guard let isAvailable = UserVM.shared.driverData[APIKeys.kIsAvailable] as? Bool, let showMultipliedDriver = UserVM.shared.driverData[APIKeys.kShowMultipliedDriver] as? Bool, let count = UserVM.shared.driverData[APIKeys.kCount] as? Int  else { return }
        let dict: JSONDictionary = [APIKeys.kIsAvailable: isAvailable, APIKeys.kLatitude: currentLocation.latitude, APIKeys.kLongitude: currentLocation.longitude, APIKeys.kDeviceToken: AppDelegate.DEVICEID, APIKeys.kShowMultipliedDriver: showMultipliedDriver, APIKeys.kCount: count]
        UserVM.shared.addDriverData(dict: dict) { [weak self] (message, error) in
            if error != nil {
                self?.showErrorMessage(error: error)
                return
            }

            DataManager.availability = UserVM.shared.driverData[APIKeys.kIsAvailable] as? Bool
            DataManager.showMultipliedDriver = UserVM.shared.driverData[APIKeys.kShowMultipliedDriver] as? Bool
            HomeVC.type = .driver
            self?.loadSideMenu()
        }
    }

    func saveUserData() {
        let dict: JSONDictionary = [APIKeys.kIsAvailable: availability, APIKeys.kLatitude: currentLocation.latitude, APIKeys.kLongitude: currentLocation.longitude, APIKeys.kDeviceToken: AppDelegate.DEVICEID, APIKeys.kShowMultipliedDriver: showMultipliedDriver, APIKeys.kCount: 0]
        UserVM.shared.addDriverData(dict: dict) { [weak self] (message, error) in
            if error != nil {
                self?.showErrorMessage(error: error! as NSError)
                return
            }
            
            DataManager.availability = self?.availability
            DataManager.showMultipliedDriver = self?.showMultipliedDriver
            HomeVC.type = .driver
            self?.loadSideMenu()
        }
    }
}
