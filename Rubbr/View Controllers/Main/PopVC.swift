//
//  PopVC.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 12/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import UIKit

class PopVC: BaseVC {
    //MARK: IBActions
    @IBAction func crossAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
