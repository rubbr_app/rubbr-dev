//
//  ButtonCell.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 01/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import UIKit

protocol ButtonCellDelegate: class {
    func buttonDidTapFor(section: Int)
}

class ButtonCell: UITableViewCell {
    @IBOutlet weak var myButton: UIButton!
    var section: Int?
    weak var delegate: ButtonCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        myButton.set(radius: 25.0)
        
        let myImage =  CustomGradientLayer.setGrad(to: myButton, color1: UIColor.RubbrColor.golden.color(), color2: UIColor.RubbrColor.orange.color(), type: .horizontal)
        myButton.setBackgroundImage(myImage, for: .normal)
    }
    
    @IBAction func buttonAction(_ sender: UIButton) {
        guard let section = section else { return }
        delegate?.buttonDidTapFor(section: section)
    }
}
