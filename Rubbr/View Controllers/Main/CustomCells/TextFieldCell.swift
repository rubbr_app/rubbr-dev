//
//  TextFieldCell.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 01/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import UIKit

class TextFieldCell: UITableViewCell {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var rightButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.set(radius: 20.0, borderColor: UIColor.RubbrColor.borderColor.color(), borderWidth: 2.0)
        textField.inputView = nil
        textField.isSecureTextEntry = false
        rightButton.tintColor = UIColor.clear
    }
    
    func setCellData(loginFieldType: UserLoginFieldType) {
        textField.tag = loginFieldType.rawValue
        textField.setPlaceholder(placholder: loginFieldType.placeHolder)
        textField.keyboardType = loginFieldType.keyboardType
        textField.isSecureTextEntry = loginFieldType.isSecureTextEntry
    }
    
    func setCellData(addressFieldType: UserAddressFieldType) {
        textField.tag = addressFieldType.rawValue + 10
        textField.keyboardType = addressFieldType.keyboardType
    }
    
    func setCellData(cardFieldType: UserCardFieldType) {
        textField.tag = cardFieldType.rawValue + 20
        textField.setPlaceholder(placholder: cardFieldType.placeHolder)
        textField.keyboardType = cardFieldType.keyboardType
        textField.isSecureTextEntry = cardFieldType.isSecureTextEntry
    }
}
