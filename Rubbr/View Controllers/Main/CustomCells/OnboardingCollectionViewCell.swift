//
//  OnboardingCollectionViewCell.swift
//  Rubbr
//
//  Created by Hamlet on 21.03.21.
//  Copyright © 2021 Hamlet Kosakyan. All rights reserved.
//

import UIKit

protocol OnboardingCollectionViewCellDelegate: class {
    func actionHandlerFor(indexPath: IndexPath)
}

class OnboardingCollectionViewCell: UICollectionViewCell {
    var indexPath: IndexPath?
    weak var delegate: OnboardingCollectionViewCellDelegate?
    
    @IBAction func gotItAction(_ sender: UIButton) {
        guard let indexPath = indexPath else { return }
        delegate?.actionHandlerFor(indexPath: indexPath)
    }
}
