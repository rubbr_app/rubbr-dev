//
//  AddressAutoloadVC.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 3/19/19.
//  Copyright © 2019 Hamlet Kosakyan. All rights reserved.
//

import UIKit
import MapKit

protocol LocationSearchDelegate: class {
    func addressSelected(address: AddressModel)
}

class AddressAutoloadVC: UIViewController {
    private var results: [MKMapItem] = []
    private var mapView: MKMapView = MKMapView()
    
    weak var delegate: LocationSearchDelegate?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var boarderView: UIView!
    @IBOutlet weak var searchIconView: UIImageView!
    @IBOutlet weak var searchTextField: UITextField!
    
    private var address: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let maskPathAll = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 10.0, height: 10.0))
        let shapeLayerAll = CAShapeLayer()
        shapeLayerAll.frame = view.bounds
        shapeLayerAll.path = maskPathAll.cgPath
        view.layer.mask = shapeLayerAll
        
        searchTextField.placeholder = "Search"
        boarderView.layer.borderWidth = 1
        boarderView.layer.borderColor = UIColor.lightGray.cgColor

        if let location = LocationManager.location {
            let circle = MKCircle(center: location.coordinate, radius: 500)
            mapView.addOverlay(circle)
            let viewRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 1200, longitudinalMeters: 1200)
            mapView.setRegion(viewRegion, animated: true)
        }
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let searchAddress = address {
            searchTextField.text = searchAddress
            makeSearch(with: searchAddress)
        }
    }
    
    func setAddress(address: String) {
        self.address = address
    }
}

extension AddressAutoloadVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // 1. textField.text.isEmpty && !string.isEmpty
        if textField.text?.isEmpty ?? true {
            let finalString = string
            makeSearch(with: finalString)
            return true
        }
        
        // 2. !textField.text.isEmpty && string.isEmpty
        if !(textField.text?.isEmpty ?? true) && string == "" {
            let finalString = String(textField.text?.dropLast() ?? "")
            makeSearch(with: finalString)
            return true
        }
        
        // 3. !textField.text.isEmpty && !string.isEmpty
        let finalString = "\(textField.text ?? "")\(string)"
        makeSearch(with: finalString)
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        makeSearch(with: "")
        return true
    }
}

extension AddressAutoloadVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let address: AddressModel = AddressModel()
        if results.count == 0 {
            if let addressString = searchTextField.text {
                address.street = addressString
            }
        } else {
            let selectedItemPlacemark = results[indexPath.row].placemark
            address.placemark = selectedItemPlacemark
        }
        delegate?.addressSelected(address: address)
    }
}

extension AddressAutoloadVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if results.count == 0 && searchTextField.text != "" {
            return 1
        }
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addressCell")!
        if results.count == 0 && searchTextField.text != "" {
            cell.textLabel?.text = searchTextField.text
            results = []
            return cell
        }
        
        let selectedItem = results[indexPath.row].placemark
        cell.textLabel?.text = parseAddress(selectedItem: selectedItem)
        return cell
    }
}

private extension AddressAutoloadVC {
    func makeSearch(with key: String) {
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = key
        request.region = mapView.region
        let search = MKLocalSearch(request: request)
        
        search.start { [weak self] response, _ in
            guard let self = self else { return }
            guard let response = response else {
                self.results = []
                self.tableView.reloadData()
                return
            }
            self.results = response.mapItems
            self.tableView.reloadData()
        }
    }
    
    func parseAddress(selectedItem:MKPlacemark) -> String {
        let firstSpace = (selectedItem.subThoroughfare != nil && selectedItem.thoroughfare != nil) ? " " : ""
        let comma = (selectedItem.subThoroughfare != nil || selectedItem.thoroughfare != nil) && (selectedItem.subAdministrativeArea != nil || selectedItem.administrativeArea != nil) ? ", " : ""

        let addressLine = String(
            format:"%@%@%@%@%@",
            // street number
            selectedItem.subThoroughfare ?? "",
            firstSpace,
            // street name
            selectedItem.thoroughfare ?? "",
            comma,
            // city
            selectedItem.locality ?? ""
        )
        return addressLine
    }
}

// MARK: - Pan modal methods
extension AddressAutoloadVC: PanModalPresentable {
    var panScrollable: UIScrollView? {
        return tableView
    }

    var anchorModalToLongForm: Bool {
        return false
    }

    var shortFormHeight: PanModalHeight {
        return .contentHeight(300)
    }

    var longFormHeight: PanModalHeight {
        return .maxHeightWithTopInset(120)
    }

    var allowsExtendedPanScrolling: Bool {
        return true
    }
}

class AddressModel {
    var placemark: MKPlacemark?
    var street: String?
}
