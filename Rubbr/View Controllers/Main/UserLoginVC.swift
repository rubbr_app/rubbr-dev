//
//  UserLoginVC.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 01/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import UIKit
import Firebase
import MapKit

enum LoginScreenType {
    case logIn
    case address
    case payment
}

enum UserLoginSections: Int {
    case first = 1
    case second = 2
    case third = 3
    
    func getInfoTuple(alreadyLoggedIn: Bool, isFromSide: Bool) -> (title: String, count: String) {
        switch self {
        case .first:
            return alreadyLoggedIn ?
                (UserLoginVC.type == .payment ?
                    ("1. Payment Info", "1 out of 1") : isFromSide ?
                        ("1. Tell us where to come", "1 out of 1")
                    : ("1. Tell us where to come", "1 out of 2"))
                : ("1. Login or Signup", "1 out of 3")
        case .second:
            return alreadyLoggedIn ? ("2. Payment Info", "2 out of 2") : ("2. Tell us where to come", "2 out of 3")
        case .third:
            return ("3. Payment Info", "3 out of 3")
        }
    }
}

class UserLoginVC: BaseVC, UIViewControllerTransitioningDelegate {

    //MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    //MARK: Variables
    var mySection = 1
    var userData: [Int: String] = [:]
    var alreadyLoggedIn = false
    weak var delegate: SendRequestDelegate?
    var amount = Double()
    var isFromSide = false
    static var type: LoginScreenType = .logIn

    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        set(title: "Your Order")
        if isFromSide {
            setMenuButton()
            descriptionLabel.text = "Delivery fee included"
        }
        setData()
    }

    private func addPullUpController(address: String?) {
        let vc = UIStoryboard(storyboard: .Home).instantiateViewController(withIdentifier: "AddressAutoloadVC") as! AddressAutoloadVC
        if let address = address {
            vc.setAddress(address: address)
        }
        vc.delegate = self
        
        vc.modalPresentationStyle = .custom
        vc.modalPresentationCapturesStatusBarAppearance = true
        vc.transitioningDelegate = PanModalPresentationDelegate.default
        super.present(vc, animated: true, completion: nil)
    }

    override func backButtonAction() {
        loadSideMenu()
    }

    private func setData() {
        if DataManager.userID != nil {
            alreadyLoggedIn = true
            getAddress()
            if let cardId = DataManager.cardId {
                getCard(cardId: cardId)
            }
        }
        amountLabel.text = "\(amount)€"
    }
}

extension UserLoginVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return mySection
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return alreadyLoggedIn ? UserLoginVC.type == .payment ? 4 : 5: 3
        case 1:
            return alreadyLoggedIn ? 4 : 5
        default:
            return 4
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            return alreadyLoggedIn ? UserLoginVC.type == .payment ? setCardData(indexPath: indexPath) : setAddressData(indexPath: indexPath): setLoginData(indexPath: indexPath)
        case 1:
            return alreadyLoggedIn ? setCardData(indexPath: indexPath) : setAddressData(indexPath: indexPath)
        default:
            return setCardData(indexPath: indexPath)
        }
    }
}

extension UserLoginVC: ButtonCellDelegate {
    func buttonDidTapFor(section: Int) {
        view.endEditing(true)
        switch section {
        case 0:
            if alreadyLoggedIn {
                if UserLoginVC.type == .payment, let dict = validateCardDetails() {
                    createCardToken(dict: dict)
                    return
                }
                
                if let dict = validateAddressData() {
                    addAddress(dict: dict)
                    return
                }
            }
                
            if let message = validateLoginData() {
                showAlert(message: message)
                return
            }
            
            login()
        case 1:
            if !alreadyLoggedIn {
                if let dict = validateAddressData() {
                    addAddress(dict: dict)
                }
                return
            }
            
            if let dict = validateCardDetails() {
                createCardToken(dict: dict)
                return
            }
        default:
            if let dict = validateCardDetails() {
                createCardToken(dict: dict)
            }
        }
    }
}

extension UserLoginVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LoginSectionCell.identifier) as? LoginSectionCell, let type = UserLoginSections(rawValue: section + 1) else { return nil }
        let tuple = type.getInfoTuple(alreadyLoggedIn: alreadyLoggedIn, isFromSide: isFromSide)
        cell.titleLabel.text = tuple.title
        cell.countLabel.text = tuple.count
 
        return cell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
}

extension UserLoginVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.placeholder == "Address" {
            addPullUpController(address: textField.text)
        }

        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        userData[textField.tag] = textField.text!

        var eventName = ""

        switch textField.placeholder {
        case kEmailFieldPlaceholder:
            eventName = kEmailEntered
        case kPassowrdFieldPlaceholder:
            eventName = kPasswordEntered
        case ADDRESS_PLACEHOLDER_ARRAY[0]:
            eventName = kAddressEntered
        case ADDRESS_PLACEHOLDER_ARRAY[1]:
            eventName = kZipCodeEntered
        case ADDRESS_PLACEHOLDER_ARRAY[2]:
            eventName = kCityEntered
        case ADDRESS_PLACEHOLDER_ARRAY[3]:
            eventName = kNameOnRingBellEntered
        case kCardNumberFieldPlaceholder:
            eventName = kCardNumberEntered
        case k3DigitNumberFieldPlaceholder:
            eventName = kCardCVVEntered
        case kExpirationDateFieldPlaceholder:
            eventName = kCardExpirationDateEntered
        default:
            break
        }
        GoogleAnalytics.logEvent(event: eventName, parameters: nil)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//MARK: Validations
extension UserLoginVC {
    func validateLoginData() -> String? {
        if let email = userData[0] { //Email Data
            if !email.isValidEmail {
                return kEmailValidation
            }
            if let password = userData[1] { //Password
                if !password.isValidPassword {
                    return kPasswordValidation
                }
                return nil
            }
            return kPasswordValidation
        }
        return kEmailValidation
    }

    func validateAddressData() -> JSONDictionary? {
        var dict = JSONDictionary()
        if let address = userData[10] { //Address
            if !address.isEmpty {
                dict[APIKeys.kAddress] = address.trimmingCharacters(in: .whitespaces)
                if let zip = userData[11] { //Zip Code
                    if zip.isValidZipCode {
                        dict[APIKeys.kZipCode] = zip.trimmingCharacters(in: .whitespaces)
                        if let city = userData[12] { //City
                            if !city.isEmpty {
                                dict[APIKeys.kCity] = city.trimmingCharacters(in: .whitespaces)
                                DataManager.userAddress = "\(address), \(city) \(zip)"
                                if let nameOnRing = userData[13] { //Name on Ring
                                    dict["Name_On_Ring"] = nameOnRing.trimmingCharacters(in: .whitespaces)
                                    DataManager.userName = nameOnRing
                                }
                                dict[APIKeys.kStripeId] = DataManager.stripeId ?? StripeVM.shared.customerID
                                return dict
                            }
                            showAlert(message: kCityValidation)
                            return nil
                        }
                        showAlert(message: kCityValidation)
                        return nil
                    }
                    showAlert(message: kZipValidation)
                    return nil
                }
                showAlert(message: kZipValidation)
                return nil
            }
            showAlert(message: kAddressValidation)
            return nil
        }
        showAlert(message: kAddressValidation)
        return nil
    }

    func validateCardDetails() -> JSONDictionary? {
        var dict = JSONDictionary()
        if let card = userData[20] { //Card Number
            if card.isValidCardNumber {
                dict[APIKeys.kCardNumber] = card
                if let cvv = userData[21] { //CVV
                    if cvv.isValidCVV {
                        dict[APIKeys.kCvv] = cvv
                        if let date = userData[22] { //ExpirationDate
                            if !date.isEmpty {
                                dict[APIKeys.kExpiryDate] = date
                                return dict
                            }
                            showAlert(message: kExpiryDateValidation)
                            return nil
                        }
                        showAlert(message: kExpiryDateValidation)
                        return nil
                    }
                    showAlert(message: kCvvValidation)
                    return nil
                }
                showAlert(message: kCvvValidation)
                return nil
            }
            showAlert(message: kCardValidation)
            return nil
        }
        showAlert(message: kCardValidation)
        return nil
    }
}

//MARK: Firebase Methods
extension UserLoginVC {
    func login() {
        UserVM.shared.loginWith(email: userData[0]!, password: userData[1]!) { [weak self] (message, error) in
            if error != nil {
                if error!.localizedDescription == kNoUser {
                    self?.signup()
                } else {
                    self?.showErrorMessage(error: error! as NSError)
                }
            } else {
                DataManager.userType = ScreenType.user.rawValue
                self?.getAddress()
            }
        }
        GoogleAnalytics.logEvent(event: kLoginButtonClicked, parameters: nil)
    }

    func signup() {
        UserVM.shared.signupWith(email: userData[0]!, password: userData[1]!) { [weak self] (message, error) in
            if error != nil {
                self?.showErrorMessage(error: error! as NSError)
            } else {
                self?.createStripeUser()
            }
        }
        GoogleAnalytics.logEvent(event: kSignUpButtonClicked, parameters: nil)
    }

    func addAddress(dict: JSONDictionary) {
        UserVM.shared.addUserAddress(dict: dict) { [weak self] (message, error) in
            guard let self = self else { return }
            if error != nil {
                self.showErrorMessage(error: error! as NSError)
            } else {
                if self.isFromSide {
                    self.showAlert(message: "Profile details updated successfully", cancelAction: { (action) in
                        self.loadSideMenu()
                    })
                    return
                } else if let cardId = DataManager.cardId {
                    self.getCard(cardId: cardId)
                } else {
                    self.mySection += 1
                    self.tableView.reloadData()
                    self.tableView.scrollToRow(at: IndexPath(row: 0, section: self.mySection - 1), at: .top, animated: true)
                }
            }
        }
    }

    func getAddress() {
        UserVM.shared.getAddress { [weak self] (message, error) in
            guard let self = self else { return }
            
            if error != nil {
                self.showErrorMessage(error: error! as NSError)
            } else {
                self.userData[10] = UserVM.shared.userAddress[APIKeys.kAddress] as? String ?? ""
                self.userData[11] = UserVM.shared.userAddress[APIKeys.kZipCode] as? String ?? ""
                self.userData[12] = UserVM.shared.userAddress[APIKeys.kCity] as? String ?? ""
                self.userData[13] = UserVM.shared.userAddress[APIKeys.kNameOnRing] as? String ?? ""
                let stripeId = UserVM.shared.userAddress[APIKeys.kStripeId] as? String ?? ""
                DataManager.stripeId = stripeId
                if !self.alreadyLoggedIn {
                    self.mySection += 1
                    self.tableView.reloadData()
                    self.tableView.scrollToRow(at: IndexPath(row: 2, section: self.mySection - 1), at: .top, animated: true)
                } else {
                    self.tableView.reloadData()
                }
            }
        }
    }

    func createStripeUser() {
        StripeVM.shared.createCustomer(email: userData[0]!) { [weak self] (message, error) in
            guard let self = self else { return }
            if error != nil {
                self.showErrorMessage(error: error)
            } else {
                self.mySection = 2
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath(row: 0, section: 1), at: .top, animated: true)
            }
        }
    }

    func createCardToken(dict: JSONDictionary) {
        let expiry = dict[APIKeys.kExpiryDate] as? String ?? ""
        let expiryArray = expiry.components(separatedBy: "/")
        var cardNumber = dict[APIKeys.kCardNumber] as? String ?? ""
        if cardNumber.contains("X") { //Card already stored in userDefaults
            cardNumber = UserDefaults.standard.value(forKey: APIKeys.kCardNumber) as! String
        } else {
            UserDefaults.standard.setValue(cardNumber, forKey: APIKeys.kCardNumber)
        }

        StripeManager.shared.saveCard(cardNumber: cardNumber, expMonth: Int(expiryArray[0].replacingOccurrences(of: " ", with: ""))!, expYear: Int(expiryArray[1].replacingOccurrences(of: " ", with: ""))!, cvv: dict[APIKeys.kCvv] as? String ?? "") { [weak self] (token, error) in
            guard let self = self else { return }
            if error != nil {
                self.showErrorMessage(error: error)
            } else {
                self.addCard(token: token!)
            }
        }
    }

    func addCard(token: String) {
        StripeVM.shared.addCard(token: token) { [weak self] (message, error) in
            guard let self = self else { return }
            if error != nil {
                self.showErrorMessage(error: error)
            } else {
                if self.isFromSide {
                    self.showAlert(message: "Profile details updated successfully", cancelAction: { (action) in
                        self.loadSideMenu()
                    })
                    return
                }
                self.delegate?.sendRequest(driver: nil, amount: nil)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }

    func getCard(cardId: String) {
        StripeVM.shared.getCard(cardID: cardId) { [weak self] (message, error) in
            guard let self = self else { return }
            
            if error != nil {
                self.showErrorMessage(error: error)
            } else {
                self.userData[20] = StripeVM.shared.cardDetails[APIKeys.kCardNumber] as? String ?? ""
                self.userData[22] = StripeVM.shared.cardDetails[APIKeys.kExpiryDate] as? String ?? ""
                if !self.isFromSide {
                    self.mySection += 1
                }
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath(row: 0, section: self.mySection - 1), at: .top, animated: true)
            }
        }
    }
}

extension UserLoginVC: LocationSearchDelegate {
    func addressSelected(address: AddressModel) {
        if let placemark = address.placemark {
            var addressNameAndNumber = ""
            if let thoroughfare = placemark.thoroughfare {
                addressNameAndNumber = thoroughfare
            }
            if let subThoroughfare = placemark.subThoroughfare {
                addressNameAndNumber = addressNameAndNumber + " " + subThoroughfare
            }
            userData[10] = addressNameAndNumber

            if let postalCode = placemark.postalAddress?.postalCode {
                userData[11] = postalCode
            }

            if let city = placemark.locality {
                userData[12] = city
            }
        } else if let street = address.street {
            userData[10] = street
        }
        tableView.reloadData()
        
        dismiss(animated: true, completion: nil)
        if !isFromSide {
            tableView.scrollToRow(at: IndexPath(row: 2, section: mySection - 1), at: .top, animated: true)
        }
    }
}

//MARK: Set Table Data
extension UserLoginVC {
    func setLoginData(indexPath: IndexPath) -> UITableViewCell {
        guard let type = UserLoginFieldType(rawValue: indexPath.row) else {
            let cell = tableView.dequeueReusableCell(withIdentifier: ButtonCell.identifier) as! ButtonCell
            cell.section = indexPath.section
            cell.myButton.setTitle("Login", for: .normal)
            cell.delegate = self
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldCell.identifier) as! TextFieldCell
        cell.setCellData(loginFieldType: type)
        cell.textField.text = userData[type.rawValue]
        cell.textField.delegate = self
        return cell
    }

    func setAddressData(indexPath: IndexPath) -> UITableViewCell {
        guard let type = UserAddressFieldType(rawValue: indexPath.row) else {
            let cell = tableView.dequeueReusableCell(withIdentifier: ButtonCell.identifier) as! ButtonCell
            cell.section = indexPath.section
            cell.delegate = self
            cell.myButton.setTitle("Confirm", for: .normal)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldCell.identifier) as! TextFieldCell
        cell.setCellData(addressFieldType: type)
        cell.textField.text = userData[indexPath.row + 10]
        cell.textField.delegate = self
        cell.textField.setPlaceholder(placholder: ADDRESS_PLACEHOLDER_ARRAY[indexPath.row])
        if type != .zip {
            cell.rightButton.addTarget(self, action: #selector(self.rightButtonAction(sender:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    func setCardData(indexPath: IndexPath) -> UITableViewCell {
        guard let type = UserCardFieldType(rawValue: indexPath.row) else {
            let cell = tableView.dequeueReusableCell(withIdentifier: ButtonCell.identifier) as! ButtonCell
            cell.section = indexPath.section
            cell.delegate = self
            let buttonTitle = isFromSide ? "Confirm" : "Get Rubber!"
            cell.myButton.setTitle(buttonTitle, for: .normal)
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldCell.identifier) as! TextFieldCell
        cell.textField.delegate = self
        
        if type == .expirationdate {
            let datePicker = MonthYearPickerView()
            datePicker.onDateSelected = { (month: Int, year: Int) in
                let string = String(format: "%02d/ %d", month, year)
                cell.textField.text = string
                self.userData[indexPath.row + 20] = string
            }
            cell.textField.inputView = datePicker
        } else {
            cell.textField.text = userData[indexPath.row + 20]
        }
        
        cell.setCellData(cardFieldType: type)
        
        return cell
    }

    @objc func rightButtonAction(sender: UIButton) {
        let controller = storyboard!.instantiateViewController(withIdentifier: "PopVC")
        controller.preferredContentSize = CGSize(width: 163, height: 69)
        showPopup(controller, sourceView: sender)
    }

    private func showPopup(_ controller: UIViewController, sourceView: UIView) {
        let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
        presentationController.sourceView = sourceView
        presentationController.sourceRect = sourceView.bounds
        presentationController.permittedArrowDirections = [.down, .up]
        presentationController.backgroundColor = UIColor(red: 235 / 255, green: 86 / 255, blue: 104 / 255, alpha: 0.8)
        present(controller, animated: true)
    }
}

struct Driver {
    var location: CLLocationCoordinate2D!
    var distance: Double!
    var id: String!
    var token: String!
}
