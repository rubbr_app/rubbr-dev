//
//  OnboardingVC.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 10/11/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import UIKit

enum OnboardingCell: Int, CaseIterable {
    case introduction = 0
    case price
    case location
    
    var identifier: String {
        switch self {
        case .introduction: return "OnboardingCollectionViewCellIntroduction"
        case .price: return "OnboardingCollectionViewCellPrice"
        case .location: return "OnboardingCollectionViewCellLocation"
        }
    }
}

class OnboardingVC: BaseVC {
    //MARK: IBOutlets
    @IBOutlet weak var onboardingCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideNavigationBar()
    }

    //MARK: IBActions
    @IBAction func driverButtonAction(_ sender: UIButton) {
    }
}

extension OnboardingVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return OnboardingCell.allCases.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let type = OnboardingCell(rawValue: indexPath.row), let cell = collectionView.dequeueReusableCell(withReuseIdentifier: type.identifier, for: indexPath) as? OnboardingCollectionViewCell else { return UICollectionViewCell() }
        cell.delegate = self
        cell.indexPath = indexPath
        return cell
    }
}

extension OnboardingVC: OnboardingCollectionViewCellDelegate {
    func actionHandlerFor(indexPath: IndexPath) {
        if indexPath.row != 2 {
            let indexPath = IndexPath(row: indexPath.row + 1, section: 0)
            onboardingCollectionView.scrollToItem(at: indexPath, at: .right, animated: true)
            return
        }
        
        getCurrentLocation { [weak self] (success) in
            DataManager.userType = ScreenType.user.rawValue
            HomeVC.type = .user
            self?.loadSideMenu()
        }
    }
}

extension OnboardingVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
}
