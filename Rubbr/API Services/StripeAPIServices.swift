//
//  StripeAPIServices.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 17/12/18.
//  Copyright © 2021 Hamlet Kosakyan. All rights reserved.
//

import Foundation

enum StripeAPI: APIService {
    case createUser(email: String)
    case addCard(token: String)
    case getCard(cardID: String)
    case makePayment(amount: Double)
    
    var path: String? {
        switch self {
        case .createUser:
            return "\(BASE_URL)customers"
        case .addCard:
            guard let stripeId = DataManager.stripeId else { return nil }
            return "\(BASE_URL)customers/\(stripeId)/sources"
        case let .getCard(cardID):
            guard let stripeId = DataManager.stripeId else { return nil }
            return "\(BASE_URL)customers/\(stripeId)/sources/\(cardID)"
        case .makePayment:
            return "\(BASE_URL)charges"
        }
    }
    
    var resource: Resource? {
        switch self {
        case let .createUser(email):
            return Resource(method: .post, parameters: [APIKeys.kEmail: email])
        case let .addCard(token):
            return Resource(method: .post, parameters: [APIKeys.kSource: token])
        case .getCard:
            return Resource(method: .get)
        case let .makePayment(amount):
            guard let cardId = DataManager.cardId, let stripeId = DataManager.stripeId else { return nil }
            return Resource(method: .post, parameters: [APIKeys.kSource: cardId, APIKeys.kCustomer: stripeId, APIKeys.kAmount: amount, APIKeys.CURRENCY: CURRENCY_EUR])
        }
    }
}

// MARK: Method for webservice
extension APIManager {
    class func createUser(email: String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        StripeAPI.createUser(email: email).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            } else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func addCard(token: String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        StripeAPI.addCard(token: token).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            } else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func getCard(cardID: String, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        StripeAPI.getCard(cardID: cardID).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            } else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func makePayment(amount: Double, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        StripeAPI.makePayment(amount: amount).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            } else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
}
