//
//  UserDataModel.swift
//  Rubbr
//
//  Created by Hamlet on 20.03.21.
//  Copyright © 2021 Hamlet Kosakyan. All rights reserved.
//

import UIKit

enum UserLoginFieldType: Int {
    case email
    case password
    
    var placeHolder: String {
        switch self {
        case .email: return kEmailFieldPlaceholder
        case .password: return kPassowrdFieldPlaceholder
        }
    }
    
    var keyboardType: UIKeyboardType {
        return self == .password ? .default : .emailAddress
    }
    
    var isSecureTextEntry: Bool {
        return self == .password
    }
}

enum UserAddressFieldType: Int {
    case address
    case zip
    case city
    case nameOnRingBell
    
    var keyboardType: UIKeyboardType {
        return self == .zip ? .numberPad : .default
    }
    
    var rightButtonImage: UIImage? {
        return self == .nameOnRingBell ? #imageLiteral(resourceName: "question") : nil
    }
}

enum UserCardFieldType: Int {
    case cardNumber
    case cvv
    case expirationdate
    
    var placeHolder: String {
        switch self {
        case .cardNumber: return kCardNumberFieldPlaceholder
        case .cvv: return k3DigitNumberFieldPlaceholder
        case .expirationdate: return kExpirationDateFieldPlaceholder
        }
    }
    
    var keyboardType: UIKeyboardType {
        return .numberPad
    }
    
    var rightButtonImage: UIImage? {
        return self == .expirationdate ? #imageLiteral(resourceName: "downArrow") : nil
    }
    
    var isSecureTextEntry: Bool {
        return self == .cvv
    }
}
