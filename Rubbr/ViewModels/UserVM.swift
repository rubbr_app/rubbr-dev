//
//  UserVM.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 01/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseAuth

class UserVM {
    static var shared = UserVM()
    private init() { }

    var userDetails: User!
    var userAddress = JSONDictionary()
    var driverData = JSONDictionary()
    var driverArray = [QueryDocumentSnapshot]()

    //MARK: Login
    func loginWith(email: String, password: String, response: @escaping responseCallBack) {
        Indicator.shared.showIndicator()
        Auth.auth().signIn(withEmail: email.trimmingCharacters(in: .whitespaces), password: password) { [weak self] (result, error) in
            guard let self = self else { return }
            if error == nil {
                self.userDetails = result!.user
                DataManager.userID = result!.user.uid
                response(kSuccess, nil)
            } else {
                response(nil, error)
            }
            Indicator.shared.hideIndicator()
        }
    }

    func logout(response: @escaping responseCallBack) {
        Indicator.shared.showIndicator()
        do {
            Indicator.shared.hideIndicator()
            _ = try Auth.auth().signOut()
            response(kSuccess, nil)
        } catch {
            Indicator.shared.hideIndicator()
            response(nil, nil)
        }
    }

    //MARK: Signup
    func signupWith(email: String, password: String, response: @escaping responseCallBack) {
        Indicator.shared.showIndicator()
        Auth.auth().createUser(withEmail: email.trimmingCharacters(in: .whitespaces), password: password) { [weak self] (result, error) in
            guard let self = self else { return }
            if error == nil {
                self.userDetails = result!.user
                DataManager.userID = result!.user.uid
                response(kSuccess, nil)
            } else {
                response(nil, error)
            }
            Indicator.shared.hideIndicator()
        }
    }

    //MARK: Save Driver Data
    func addDriverData(dict: JSONDictionary, response: @escaping responseCallBack) {
        Indicator.shared.showIndicator()
        let db = Firestore.firestore()
        guard let userID = DataManager.userID else { return }
        db.collection(APIKeys.kDriverDetails).document(userID).setData(dict) { [weak self] (error: Error?) in
            Indicator.shared.hideIndicator()
            if let error = error {
                response(nil, error)
            } else {
                self?.driverData = dict
                response(kSuccess, nil)
            }
        }
    }

    //MARK: Get Driver Details
    func getDriverData(response: @escaping responseCallBack) {
        Indicator.shared.showIndicator()
        let db = Firestore.firestore()
        guard let userID = DataManager.userID else { return }
        db.collection(APIKeys.kDriverDetails).document(userID).getDocument { [weak self] (snapShot, error) in
            Indicator.shared.hideIndicator()
            if let error = error {
                response(nil, error)
            } else {
                if snapShot!.data() == nil {
                    response(nil, nil)
                } else {
                    self?.driverData = snapShot!.data() ?? JSONDictionary()
                    response(kSuccess, nil)
                }
            }
        }
    }

    func getAllDrivers(response: @escaping responseCallBack) {
        Indicator.shared.showIndicator()
        let db = Firestore.firestore()
        db.collection(APIKeys.kDriverDetails).getDocuments { [weak self] (snapShot, error) in
            Indicator.shared.hideIndicator()
            if let error = error {
                response(nil, error)
            } else {
                if snapShot!.documents.count == 0 {
                    response(nil, nil)
                } else {
                    print(snapShot!.documents)
                    self?.driverArray = snapShot!.documents
                    response(kSuccess, nil)
                }
            }
        }
    }

    //MARK: Save User Address
    func addUserAddress(dict: JSONDictionary, response: @escaping responseCallBack) {
        Indicator.shared.showIndicator()
        let db = Firestore.firestore()
        guard let userID = DataManager.userID else { return }
        db.collection(APIKeys.kUserAddress).document(userID).setData(dict) { (error: Error?) in
            Indicator.shared.hideIndicator()
            if let error = error {
                response(nil, error)
            } else {
                response(kSuccess, nil)
            }
        }
    }

    //MARK: Get User Address
    func getAddress(response: @escaping responseCallBack) {
        Indicator.shared.showIndicator()
        let db = Firestore.firestore()
        guard let userID = DataManager.userID else { return }
        db.collection(APIKeys.kUserAddress).document(userID).getDocument { [weak self] (snapShot, error) in
            Indicator.shared.hideIndicator()
            if let error = error {
                response(nil, error)
            } else {
                self?.userAddress = snapShot?.data() ?? JSONDictionary()
                response(kSuccess, nil)
            }
        }
    }
}
