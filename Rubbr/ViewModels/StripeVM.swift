//
//  StripeVM.swift
//  Rubbr
//
//  Created by Hamlet Kosakyan on 17/12/18.
//  Copyright © 2018 Hamlet Kosakyan. All rights reserved.
//

import Foundation

class StripeVM {
    static var shared = StripeVM()
    private init() {}
    
    var customerID = String()
    var cardID = String()
    var cardDetails = JSONDictionary()

    func createCustomer(email: String, response: @escaping responseCallBack) {
        APIManager.createUser(email: email, successCallback: { [weak self] (responseDict) in
            let message = responseDict[APIKeys.kMessage] as? String ?? APIManager.OTHER_ERROR
            self?.parseCustomerData(response: responseDict)
            response(message, nil)
        }) { (errorReason, error) in
            response(nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    func addCard(token: String, response: @escaping responseCallBack) {
        APIManager.addCard(token: token, successCallback: { [weak self] (responseDict) in
            let message = responseDict[APIKeys.kMessage] as? String ?? APIManager.OTHER_ERROR
            self?.parseCardData(response: responseDict)
            response(message, nil)
        }) { (errorReason, error) in
            response(nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    func getCard(cardID: String, response: @escaping responseCallBack) {
        APIManager.getCard(cardID: cardID, successCallback: { [weak self] (responseDict) in
            let message = responseDict[APIKeys.kMessage] as? String ?? APIManager.OTHER_ERROR
            self?.parseCardData(response: responseDict)
            response(message, nil)
        }) { (errorReason, error) in
            response(nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    func makePayment(amount: Double, response: @escaping responseCallBack) {
        APIManager.makePayment(amount: amount, successCallback: { (responseDict) in
            let message = responseDict[APIKeys.kMessage] as? String ?? APIManager.OTHER_ERROR
            response(message, nil)
        }) { (errorReason, error) in
            response(nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
}

extension StripeVM {
    func parseCustomerData(response: JSONDictionary) {
        customerID = response[APIKeys.kId] as! String
        DataManager.stripeId = customerID
    }

    func parseCardData(response: JSONDictionary) {
        cardID = response[APIKeys.kId] as! String
        let last4 = "XXXXXXXXXXXX\(response[APIKeys.kLast4] as? String ?? "XXXX")"
        let expMonth = response[APIKeys.kExpMonth] as? Int ?? 0
        let expYear = response[APIKeys.kExpYear] as? Int ?? 0
        cardDetails = [APIKeys.kCardNumber: last4, APIKeys.kExpiryDate: "\(expMonth)/\(expYear)"]
        DataManager.cardId = cardID
    }
}
